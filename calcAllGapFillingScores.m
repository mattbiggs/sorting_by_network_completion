function [fitScores] = calcAllGapFillingScores(smaller_model_list,orphanRxnSet,groupAssignments)
% Calculate a score for each reaction and each model that quantifies
% how well a reaction "fits" into a given model (i.e. how many dead-end
% metabolites are fixed)
%
% Written by Matt Biggs, April 2015

fitScores = zeros(length(orphanRxnSet),length(smaller_model_list));
for r = 1:length(orphanRxnSet)
    for m = 1:length(smaller_model_list)
        rxnSubstratesThatAreDeadEnd = sum(ismember( orphanRxnSet{r}.mets(orphanRxnSet{r}.S < 0), smaller_model_list{m}.notConsumed ));
        totalRxnSubstrates = sum(orphanRxnSet{r}.S < 0);
        rxnProductsThatAreDeadEnd = sum(ismember( orphanRxnSet{r}.mets(orphanRxnSet{r}.S > 0), smaller_model_list{m}.notProduced ));
        totalRxnProducts = sum(orphanRxnSet{r}.S > 0);
        fitScores(r,m) = rxnSubstratesThatAreDeadEnd/totalRxnSubstrates + rxnProductsThatAreDeadEnd/totalRxnProducts;
    end
end

% Take the sum of fitness scores within a group and apply to each group
% member
if max(groupAssignments) ~= length(groupAssignments) % if group size > one
    for i = 1:max(groupAssignments)
       group_i = groupAssignments == i;
       fitScores(group_i) = repmat( sum(fitScores(group_i), 1), [sum(group_i),1] );
    end
end

end