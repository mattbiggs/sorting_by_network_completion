function [] = cluster_by_network_completeness_rxn_essentiality_screen(species_models, reps, dir)
% Assign orphan reactions to models based on gaps/dead-end metabolites
% species_models is a cell array of COBRA-format metabolic models.
% Check the ability to recapitulate the reaction essentiality predictions
% of the base model against a gap-filled model and a SoNeC-processed
% model. The goal is to evaluate if SoNeC helps to improve the reaction
% essentiality predictions over normal gap-filling.
%
% Written by Matt Biggs, August 2015
initCobraToolbox()

biomassMets = {'cpd00023'; 'cpd00053'; 'cpd00035'; 'cpd00039'; 'cpd00107'; 'cpd00041'; 'cpd00132'; 'cpd00156'; 'cpd00322'; 'cpd00051'; 'cpd00033'; ...
               'cpd00066'; 'cpd00054'; 'cpd00161'; 'cpd00069'; 'cpd00129'; 'cpd00060'; 'cpd00119'; 'cpd00065'; 'cpd00381'; 'cpd00006'; 'cpd00002'; ...
               'cpd00003'; 'cpd00015'; 'cpd00126'; 'cpd00018'; 'cpd00091'; 'cpd00046'; 'cpd00773'; 'cpd00294'; 'cpd00298'; 'cpd00296'; 'cpd00206'; ...
               'cpd00557'; 'cpd11313'; 'cpd00504'; 'cpd02611'; 'cpd00122'; 'cpd03671'; 'cpd00087'; 'cpd00264'; 'cpd00214'; 'cpd00118'};

numMods = 10;
rxnGroupSize = 25;       
fractionRxns2remove = 0.15;
essentialRxnOverlap = zeros(reps,14); % pre-SoNeC overlap; post-SoNeC overlap

for r = 1:reps
    if mod(r,50) == 0
       r 
    end
    % ---------------------------------------------------------------------------
    % Remove random subset of reactions from each model, record "model-of-origin"
    % ---------------------------------------------------------------------------    
    species_subset = species_models( randperm(length(species_models),numMods) );
    orphanRxnSet = cell(0,1);
    orphanGroupAssignments = [];
    smaller_model_list = cell(size(species_subset));

    maxGroupID = 0;
    for i = 1:length(species_subset)
        % Remove appropriate fraction of model
            numRxns2remove = floor(fractionRxns2remove*length(species_subset{i}.rxns));
            if numRxns2remove < rxnGroupSize
                numRxns2remove = rxnGroupSize;
            end
        [newSmallerModel, rxnObjects, groupAssignments] = removeNreactions(species_subset{i},i,numRxns2remove,rxnGroupSize,maxGroupID);
        maxGroupID = max(groupAssignments);
        smaller_model_list{i} = newSmallerModel;
        orphanRxnSet = [orphanRxnSet ; rxnObjects];
        orphanGroupAssignments = [orphanGroupAssignments; groupAssignments];
    end

    % ---------------------------------------------------------------------------
    % Identify dead-end metabolites in models (root-no-production and
    % root-no-consumption).
    % ---------------------------------------------------------------------------
    for i = 1:length(species_subset)
       [notConsumed,notProduced] = findDeadEndMets(smaller_model_list{i});
       smaller_model_list{i}.notProduced = notProduced;
       smaller_model_list{i}.notConsumed = notConsumed;
    end

    % ---------------------------------------------------------------------------
    % Identify overlap in dead-end metabolites (in models) and
    % substrates/products (in reaction set)
    % ---------------------------------------------------------------------------    
    scores = zeros(max(orphanGroupAssignments),numMods);
    for g = 1:max(orphanGroupAssignments)
        tmpGroup = orphanRxnSet(orphanGroupAssignments==g);
        for mb = 1:numMods
            tmpModel = smaller_model_list{mb};
            [fitScore] = calcSingleGapFillingScore_nonrandom_rxn_selection(tmpModel,tmpGroup,rxnGroupSize);
            scores(g,mb) = fitScore;
        end    
    end
    scoreMaxima = rowiseMax(scores);
    groupsAssignedToFirstModel = find(scoreMaxima(:,1));
    
    % ---------------------------------------------------------------------------
    % Add reactions assigned to first model by SoNeC 
    % ---------------------------------------------------------------------------
    reformattedSmallModel = rmfield(smaller_model_list{1},{'rxnNotes','rxnReferences','ReactionsWithMetNames', ...
                                                       'KEGGSubsys','PubChemID','CheBIID','InchIString','mTBN','mTBN_alt',...
                                                       'mSim','proteins'});
    for rol = 1:length(groupsAssignedToFirstModel)
        sum(orphanGroupAssignments==groupsAssignedToFirstModel(rol))
        [reformattedSmallModel] = addRxnObjectsToModel(reformattedSmallModel,orphanRxnSet(orphanGroupAssignments==groupsAssignedToFirstModel(rol)));
    end
    
    % ---------------------------------------------------------------------------
    % Gap-fill model with reactions removed and the SoNeC-completed model
    % ---------------------------------------------------------------------------  
    fullModel = species_subset{1};
    preSoNeCmodel = smaller_model_list{1};
    postSoNeCmodel = reformattedSmallModel;
    
    fullModel_gf = SMILEY_gap_fill(fullModel, biomassMets, 0.1667);
    cp_fullM = canProduceMets( fullModel_gf,biomassMets );
    preSoNeCmodel_gf = SMILEY_gap_fill(preSoNeCmodel, biomassMets, 0.1667);
    cp_preSM = canProduceMets( preSoNeCmodel_gf,biomassMets );
    postSoNeCmodel_gf = SMILEY_gap_fill(postSoNeCmodel, biomassMets, 0.1667);
    cp_postSM = canProduceMets( postSoNeCmodel_gf,biomassMets );
    
    canProduce = biomassMets(cp_postSM > 0.01 & cp_preSM > 0.01);
    fullModel_gf.S(:,find(fullModel_gf.c)) = 0;
    preSoNeCmodel_gf.S(:,find(preSoNeCmodel_gf.c)) = 0;
    postSoNeCmodel_gf.S(:,find(postSoNeCmodel_gf.c)) = 0;
    for i = 1:length(canProduce)
        curMet = [canProduce{i} '[c]'];
        fullModel_gf.S(ismember(fullModel_gf.mets,curMet),find(fullModel_gf.c)) = -1;
        preSoNeCmodel_gf.S(ismember(preSoNeCmodel_gf.mets,curMet),find(preSoNeCmodel_gf.c)) = -1;
        postSoNeCmodel_gf.S(ismember(postSoNeCmodel_gf.mets,curMet),find(postSoNeCmodel_gf.c)) = -1;
    end
    
    % ---------------------------------------------------------------------------
    % Calculate reaction essentiality for the full model, the
    % reactions-removed model, and the SoNeC-completed model
    % ---------------------------------------------------------------------------  
    % Full model
    essentialRxns_ref = zeros( size(fullModel_gf.rxns) );
    for i = 1:length(essentialRxns_ref)
        ref_tmp = fullModel_gf;
        ref_tmp.lb(i) = 0;
        ref_tmp.ub(i) = 0;
        sol = optimizeCbModel(ref_tmp);
        essentialRxns_ref(i) = sol.f;
    end
    essential_ref = fullModel_gf.rxns(essentialRxns_ref==0);
    nonessential_ref = fullModel_gf.rxns(essentialRxns_ref~=0);
    
    % Pre-SoNeC model
    essentialRxns_pre = zeros( size(preSoNeCmodel_gf.rxns) );
    for i = 1:length(essentialRxns_pre)
        pre_tmp = preSoNeCmodel_gf;
        pre_tmp.lb(i) = 0;
        pre_tmp.ub(i) = 0;
        sol = optimizeCbModel(pre_tmp);
        essentialRxns_pre(i) = sol.f;
    end
    essential_pre = preSoNeCmodel_gf.rxns(essentialRxns_pre==0);
    
    % Post-SoNeC model
    essentialRxns_post = zeros( size(postSoNeCmodel_gf.rxns) );
    for i = 1:length(essentialRxns_post)
        post_tmp = postSoNeCmodel_gf;
        post_tmp.lb(i) = 0;
        post_tmp.ub(i) = 0;
        sol = optimizeCbModel(post_tmp);
        essentialRxns_post(i) = sol.f;
    end
    essential_post = postSoNeCmodel_gf.rxns(essentialRxns_post==0);
    
    filename1 = [dir 'reaction_essentiality_predictions_rep_' num2str(r) '.tsv'];
    fid = fopen(filename1,'w');
    fprintf(fid,['essential_ref\t' strjoin(essential_ref','\t') '\n']);
    fprintf(fid,['nonessential_ref\t' strjoin(nonessential_ref','\t') '\n']);
    fprintf(fid,['essential_pre\t' strjoin(essential_pre','\t') '\n']);
    fprintf(fid,['essential_post\t' strjoin(essential_post','\t') '\n']);
    fclose(fid);

    % ---------------------------------------------------------------------------
    % Calculate reaction essentiality overlap between the full model, the
    % reactions-removed model, and the SoNeC-completed model
    % ---------------------------------------------------------------------------
    pre_TP = sum(ismember(essential_ref,essential_pre));
    pre_FP = sum(ismember(nonessential_ref,essential_pre));
    pre_TN = sum(~ismember(nonessential_ref,essential_pre)); 
    pre_FN = sum(~ismember(essential_ref,essential_pre));
    pre_accuracy = (pre_TP + pre_TN) / (pre_TP + pre_TN + pre_FP + pre_FN);
    pre_precision = pre_TP / (pre_TP + pre_FP);
    pre_recall = pre_TP / (pre_TP + pre_FN);
    
    post_TP = sum(ismember(essential_ref,essential_post));
    post_FP = sum(ismember(nonessential_ref,essential_post));
    post_TN = sum(~ismember(nonessential_ref,essential_post)); 
    post_FN = sum(~ismember(essential_ref,essential_post));
    post_accuracy = (post_TP + post_TN) / (post_TP + post_TN + post_FP + post_FN);
    post_precision = post_TP / (post_TP + post_FP);
    post_recall = post_TP / (post_TP + post_FN);
    
    essentialRxnOverlap(r,:) = [pre_TP pre_FP pre_TN pre_FN pre_accuracy pre_precision pre_recall post_TP post_FP post_TN post_FN post_accuracy post_precision post_recall];
    
    % ---------------------------------------------------------------------------
    % Check assignment accuracy
    % ---------------------------------------------------------------------------
    % Write to a file that can be read into R for plotting
    filename = [dir 'reaction_essentiality_overlap_reps_' num2str(reps) '.tsv'];
    dlmwrite(filename,essentialRxnOverlap,'\t');
end



end

