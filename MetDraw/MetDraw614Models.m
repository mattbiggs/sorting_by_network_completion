%% Writing SBML files and setting up .mets files for MetDraw
%
% Written by Matt Biggs, June 2015 - adapted from code by Jennifer Bartell ("metdrawmodels.m")
load m614ab
load seed_smat

% Make joint model with reactions from both (for visualization purposes)
modelM = m614a_gf;
unique2before = ~ismember(m614b_gf.rxns,m614a_gf.rxns);
modelM.S = [modelM.S m614b_gf.S(:,unique2before)];
modelM.rxns = [modelM.rxns; m614b_gf.rxns(unique2before)];
modelM.rxnNames = [modelM.rxnNames; m614b_gf.rxnNames(unique2before)];
modelM.subSystems = [modelM.subSystems; m614b_gf.subSystems(unique2before)];
modelM.lb = [modelM.lb; m614b_gf.lb(unique2before)];
modelM.ub = [modelM.ub; m614b_gf.ub(unique2before)];
modelM.rev = [modelM.rev; m614b_gf.rev(unique2before)];
modelM.c = [modelM.c; m614b_gf.c(unique2before)];
modelM.rxnGeneMat = [modelM.rxnGeneMat; m614b_gf.rxnGeneMat(unique2before,:)];
modelM.rules = [modelM.rules; m614b_gf.rules(unique2before)];
modelM.grRules = [modelM.grRules; m614b_gf.grRules(unique2before)];
modelM.rxnECNumbers = [modelM.rxnECNumbers; m614b_gf.rxnECNumbers(unique2before)];
modelM.notes = cell(size(modelM.rxns));
modelM.confidenceScores = cell(size(modelM.rxns));

usedMets = sum(abs(modelM.S),2) > 0;
modelM.S = modelM.S(usedMets,:);
modelM.mets = modelM.mets(usedMets);
modelM.metNames = modelM.metNames(usedMets);
modelM.metFormulas = modelM.metFormulas(usedMets);
modelM.metKEGGids = modelM.metKEGGids(usedMets);
modelM.b = modelM.b(usedMets);
modelM.description='strain1_viz_model';

% Format model for conversion to SBML
for rc=1:length(modelM.rxns)
    % Keep only first elemnt of subsystem list
    if sum(ismember(seed_rxns_mat.rxns,modelM.rxns{rc})) > 0
        curSubsys = seed_rxns_mat.rxnKEGGmaps{ismember(seed_rxns_mat.rxns,modelM.rxns{rc})};
        cssparts = strsplit(curSubsys,';');

        modelM.subSystems{rc} = cssparts{1};
    elseif ~isempty(strfind(modelM.rxns{rc}(1:3),'Ex_'))
        modelM.subSystems{rc}='Exchange';
    else
        modelM.subSystems{rc}='None';
    end
    
    % Replace '_' with ' '
    modelM.subSystems{rc} = strrep(modelM.subSystems{rc},'_',' ');
            
    modelM.notes(rc)= strcat('SUBSYSTEM:',{' '},modelM.subSystems{rc});
end

modelM.rxnReferences=cell(size(modelM.rxns));
modelM.rxnNotes=cell(size(modelM.rxns));

% Write to SBML
writeCbToSBML(modelM,strcat(modelM.description,'.xml'));

% Write .csv file with reaction essentiality data
% 1 = Essential before SoNeC
% 2 = Essential after SoNeC
% 3 = Essential before and after
% 4 = Added by SoNeC
% 5 = Added during gap-filling
rxnsAddedByGapFilling = modelM.rxns(~ismember(modelM.rxns,m614a.rxns));
essentiality = zeros(size(modelM.rxns));
rxnOrigin = zeros(size(modelM.rxns));
for i = 1:length(essentiality)
   if sum(ismember(unique_b, modelM.rxns{i})) > 0
       essentiality(i) = 1;
   elseif sum(ismember(unique_a, modelM.rxns{i})) > 0
       essentiality(i) = 2;
   elseif sum(ismember(shared, modelM.rxns{i})) > 0
       essentiality(i) = 3;
   end
   
   if sum(ismember(addedBySoNeC, modelM.rxns{i})) > 0
       rxnOrigin(i) = 1;
   elseif sum(ismember(rxnsAddedByGapFilling, modelM.rxns{i})) > 0
       rxnOrigin(i) = 2;
   end
end

fid = fopen('strain1_reaction_essentiality.csv','w');
for i = 1:length(essentiality)
    fprintf(fid,['R_' modelM.rxns{i} ',' num2str(essentiality(i)) '\n']);
end
fclose(fid);

fid = fopen('strain1_reaction_origin.csv','w');
for i = 1:length(essentiality)
    fprintf(fid,['R_' modelM.rxns{i} ',' num2str(rxnOrigin(i)) '\n']);
end
fclose(fid);

%% Run above section, then below python code, then another processing step for cofactor ID
% must edit SBML files and copy them to MetDraw directory (or navigate to
% containing folder in command terminal (easier to keep separate for now)
% >> add outside="e" as indicated below

% <listOfCompartments>
%       <compartment id="c" name="Cytoplasm" outside="e"/>
%       <compartment id="e" name="Extracellular"/>
%     </listOfCompartments>
 
% To run MetDraw
% -Add model to <MetDrawDir>\src\SoNeC
% -Navigate to <MetDrawDir>\src\SoNeC at command prompt
% -Output xml.mets file and ID minor metabolites w asterisk 
% 
%     python ..\metdraw.py --count_mets strain1_viz_model.xml
% 
%         xml.mets example list
%             N1   *Met1
%             N2   *Met2
%             N3   Met3


% -Create model via command prompt
% python ..\metdraw.py strain1_viz_model.xml -M strain1_viz_model.xml.mets --show --Ln 5000 --output svg -p"CLUSTER_SUBSYSTEMS=False, METABOLITE_LABEL_TRANSFORM=lambda x: x[2:-2], REACTION_LABEL_TRANSFORM=lambda x: x[2:],MAX_MINORS=12" 

% To overlay data - no header in .csv, column 1 = rxn_id + 1 column / column 2 = condition of data
% python ..\metcolor.py strain1_viz_model.xml.dot.svg strain1_reaction_essentiality.csv
