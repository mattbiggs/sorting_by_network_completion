# Change Pathway Colors in a metdraw .svg file
# Essentially a find-and-replace operation, but in script form

import sys

try:
	inputFileName = sys.argv[1]
except IndexError:
	print 'USAGE: <input file name>'
	
inFile = open(inputFileName,'r')
outFile = open('color_change_rxn_origin_'+inputFileName,'w')

# See http://www.rapidtables.com/web/color/RGB_Color.htm for colors

def replaceColors (line,oldColor,newColor,changeWidth):
	newLine = line.replace('stroke='+oldColor,'stroke='+newColor)
	newLine = newLine.replace('fill='+oldColor,'fill='+newColor)
	if newLine.find('stroke='+newColor) > -1 and changeWidth == 1:
			newLine = newLine.replace('stroke-width="8"','stroke-width="20"')
			newLine = newLine.replace('stroke-width:8','stroke-width:20')
	return(newLine)
	
for line in inFile:
	if line.find('<ns0:path') > -1 or line.find('<ns0:polygon') > -1:
		# Reactions from original cluster: Black
		oldColor = '"rgb(0,0,255)"'
		newColor = '"rgb(3,3,3)"'
		newLine = replaceColors(line,oldColor,newColor,0)
		# Reactions added by SoNeC: Gold
		oldColor = '"rgb(255,255,255)"'
		newColor = '"rgb(255,215,0)"'
		newLine = replaceColors(newLine,oldColor,newColor,1)
		# Gap-filled Reactions: Lime Green
		oldColor = '"rgb(255,0,0)"'
		newColor = '"rgb(50,205,50)"'
		newLine = replaceColors(newLine,oldColor,newColor,1)
	
		outFile.write(newLine)
	else:
		outFile.write(line)

inFile.close()
outFile.close()

