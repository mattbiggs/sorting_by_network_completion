# Change Pathway Colors in a metdraw .svg file
# Essentially a find-and-replace operation, but in script form

import sys

try:
	inputFileName = sys.argv[1]
except IndexError:
	print 'USAGE: <input file name>'
	
inFile = open(inputFileName,'r')
outFile = open('color_change_rxn_essentiality_'+inputFileName,'w')

# See http://www.rapidtables.com/web/color/RGB_Color.htm for colors

def replaceColors (line,oldColor,newColor,changeWidth):
	newLine = line.replace('stroke='+oldColor,'stroke='+newColor)
	newLine = newLine.replace('fill='+oldColor,'fill='+newColor)
	if newLine.find('stroke='+newColor) > -1 and changeWidth == 1:
			newLine = newLine.replace('stroke-width="8"','stroke-width="20"')
			newLine = newLine.replace('stroke-width:8','stroke-width:20')
	return(newLine)

for line in inFile:
	if line.find('<ns0:path') > -1 or line.find('<ns0:polygon') > -1:
				
		# Non-essential: Black
		oldColor = '"rgb(0,0,255)"'
		newColor = '"rgb(0,0,0)"'
		newLine = replaceColors(line,oldColor,newColor,0)
		# Essential before SoNeC: Dodger Blue
		oldColor = '"rgb(85,85,255)"'
		newColor = '"rgb(30,144,255)"'
		newLine = replaceColors(newLine,oldColor,newColor,1)
		# Essential after SoNeC: Firebrick
		oldColor = '"rgb(255,212,212)"'
		newColor = '"rgb(255,1,1)"'
		newLine = replaceColors(newLine,oldColor,newColor,1)
		# Essential before and after SoNeC: Dark Orchid
		oldColor = '"rgb(255,0,0)"'
		newColor = '"rgb(153,50,204)"'
		newLine = replaceColors(newLine,oldColor,newColor,1)

		outFile.write(newLine)
	else:
		outFile.write(line)

inFile.close()
outFile.close()

