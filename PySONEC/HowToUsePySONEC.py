# Example code: How to use the PySONEC functions
#
# Written by Matt Biggs, October 2015

# First you'll need the COBRA toolbox for Python 
# https://cobrapy.readthedocs.org/en/latest/getting_started.html
# and the Numpy toolbox
# http://www.numpy.org/
# and the SciPy toolbox
# http://scipy.org/

import cobra
import PySONEC

# Read in models (from Matlab objects, in this case)
Mba = cobra.io.load_matlab_model("Baphidicola.mat") 			# Mba is the B. aphidicola model minus two reactions
BaSc4rxns = cobra.io.load_matlab_model("FourRxns.mat") 			# FourRxns contains two missing reactions from Mba and two from Msc
Msc = cobra.io.load_matlab_model("ScoelicolorA3.mat") 			# Msc is the S. coelicolor A3 model

# Convert to Array-based Models
Mba = cobra.ArrayBasedModel(Mba)
BaSc4rxns = cobra.ArrayBasedModel(BaSc4rxns)
Msc = cobra.ArrayBasedModel(Msc)

# Make sure they were read in properly (558, 4, and 1167 reactions, respectively)
print 'B. aphidicola model has ' + str(len(Mba.reactions)) + ' reactions'
print 'Four model has ' + str(len(BaSc4rxns.reactions)) + ' reactions'
print 'S. coelicolor A3 model has ' + str(len(Msc.reactions)) + ' reactions'

# Make a list which contains the full models
modelList = [Mba, Msc]
# Make a list of reaction groups
rxn1 = [BaSc4rxns.reactions[0]] # Single reaction (group size = 1)
rxn2 = [BaSc4rxns.reactions[1]] # Single reaction (group size = 1)
group1 = [BaSc4rxns.reactions[0], BaSc4rxns.reactions[1]] # Both reactions from Mba (group size = 2)
rxn3 = [BaSc4rxns.reactions[2]] # Single reaction (group size = 1)
rxn4 = [BaSc4rxns.reactions[3]] # Single reaction (group size = 1)
group2 = [BaSc4rxns.reactions[2], BaSc4rxns.reactions[3]] # Both reactions from Msc (group size = 2)
groupList = [rxn1, rxn2, group1, rxn3, rxn4, group2]

# Run SONEC
print 'Run SONEC'
assignments,rawScores = PySONEC.SONEC(groupList,modelList)
print 'SONEC done'

print 'rxn1 is assigned to model ' + str(assignments[0])
print 'rxn2 is assigned to model ' + str(assignments[1])
print 'group1 is assigned to model ' + str(assignments[2])
print 'rxn3 is assigned to model ' + str(assignments[3])
print 'rxn4 is assigned to model ' + str(assignments[4]) + ' (i.e. not assigned due to ambiguity in connectivity score)'
print 'group2 is assigned to model ' + str(assignments[5])