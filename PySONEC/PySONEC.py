# Implementation of the SOrting by NEtwork Completion (SONEC) algorithm
#
# Written by Matt Biggs, October 2015

import cobra
import numpy
from scipy import sparse

#-----------------------------------------------------
# Identify dead-end metabolites for each model
#-----------------------------------------------------
def findDeadEndMets(model):
	# Get the S-matrix 
	S = numpy.asarray(model.S.todense())
	
	# Identify reversible reactions
	revs = []
	nrevs = []
	for rxn in model.reactions:
		if rxn.lower_bound < 0 and rxn.upper_bound > 0:
			revs.append(model.reactions.index(rxn))	
		else:
			nrevs.append(model.reactions.index(rxn))	
			
	# Duplicate and negate reversible reactions	
	Sr = -S[:,revs]  # Reversible reactions
	Snr = S[:,nrevs] # Non-reversible reactions
	mS = numpy.concatenate( (S, Sr), axis=1) 
	
	# Find metabolites that are not consumed or not produced
	nc = sum(mS.T < 0) == 0
	nc = numpy.where(nc)[0].tolist() # Not consumed
	np = sum(mS.T > 0) == 0
	np = numpy.where(np)[0].tolist() # Not produced
	
	# Ignore completely unused metabolites
	unusedMets = [m for m in nc if m in np]
	nc = [m for m in nc if m not in unusedMets]
	np = [m for m in np if m not in unusedMets]
	
	# Identify metabolites from lone reversible reactions
	singleRevRxnMets = sum(numpy.absolute(Sr.T)) == 1
	singleRevRxnMets = numpy.where(singleRevRxnMets)[0].tolist()
	zeroNonRevRxnMets = sum(numpy.absolute(Snr.T)) == 0
	zeroNonRevRxnMets = numpy.where(zeroNonRevRxnMets)[0].tolist()
	loneRevRxnMetIndices = [m for m in singleRevRxnMets if m in zeroNonRevRxnMets]
	nc += loneRevRxnMetIndices
	nc = list(set(nc))
	np += loneRevRxnMetIndices
	np = list(set(np))
	
	# Return metabolite lists as part of the model
	model.notConsumed = [model.metabolites[x].id for x in nc]
	model.notProduced = [model.metabolites[x].id for x in np]	
	return model

#-----------------------------------------------------
# Identify group substrates and products
#-----------------------------------------------------
def findSubsNProds(group):
	for rxn in group:
		substrates = []
		r_substrates = []
		products = []
		r_products = []
		reversible = False
		
		# Check reversibility
		if rxn.lower_bound < 0 and rxn.upper_bound > 0:
			reversible = True
		
		# Get forward substrates and products
		substrates = [m.id for m in rxn.metabolites if rxn.metabolites[m] < 0]
		products = [m.id for m in rxn.metabolites if rxn.metabolites[m] > 0]
		
		# If reversible, get reverse substrates and products
		if reversible:
			r_substrates = products
			r_products = substrates
		
		# Add to the reaction object
		rxn.sbstrts = substrates
		rxn.prodcts = products
		rxn.reversible = reversible
		rxn.r_sbstrts = r_substrates
		rxn.r_prodcts = r_products
	return group

#-----------------------------------------------------
# Calculates a metabolite connectivity score for a 
# reaction with respect to a model
#-----------------------------------------------------
def calcMetConnScore(group,model):
	fitScores = []
	for rxn in group:
		rxnSubstratesThatAreDeadEnd = len([m for m in rxn.sbstrts if m in model.notConsumed])
		totalRxnSubstrates = len(rxn.sbstrts)
		rxnProductsThatAreDeadEnd = len([m for m in rxn.prodcts if m in model.notProduced])
		totalRxnProducts = len(rxn.prodcts)
		forwardFitScore = float(rxnSubstratesThatAreDeadEnd)/totalRxnSubstrates + float(rxnProductsThatAreDeadEnd)/totalRxnProducts
		
		# If the reaction is reversible, calculate the score in the reverse direction
		if rxn.reversible:
			rxnSubstratesThatAreDeadEnd = len([m for m in rxn.r_sbstrts if m in model.notConsumed])
			totalRxnSubstrates = len(rxn.r_sbstrts)
			rxnProductsThatAreDeadEnd = len([m for m in rxn.r_prodcts if m in model.notProduced])
			totalRxnProducts = len(rxn.r_prodcts)
			reverseFitScore = float(rxnSubstratesThatAreDeadEnd)/totalRxnSubstrates + float(rxnProductsThatAreDeadEnd)/totalRxnProducts
			fitScores.append(max(forwardFitScore,reverseFitScore))
		else:
			fitScores.append(forwardFitScore)			
			
	return sum(fitScores)
	
#-----------------------------------------------------	
# The SONEC algorithm: Assigns each group of reactions to the 
# model with the maximum metabolite connectivity score
#-----------------------------------------------------	
def SONEC(groupList,modList):
	MCscores = numpy.zeros((len(groupList),len(modList)))
	
	for g in groupList:
		gn = findSubsNProds(g)
		for mod in modList:
			# Identify dead-end metabolites in each model
			mod2 = findDeadEndMets(mod)
			
			# Calculate connectivity score for group g and model mod
			MCscores[groupList.index(g),modList.index(mod)] = calcMetConnScore(gn,mod2)
	
	# Choose assignments based on maximum connectivity score across rows
	assignments = []
	for i in range(len(groupList)):
		maxScore = max(MCscores[i,:])
		indicesOfMax = [(i+1) for i, j in enumerate(MCscores[i,:]) if j == maxScore]			
		if len(indicesOfMax) == 1:
			assignments.append(indicesOfMax[0])
		else:
			assignments.append(0)		

	return (assignments,MCscores)

