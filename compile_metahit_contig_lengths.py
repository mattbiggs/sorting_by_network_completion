# Compiling contig lengths

metahit_contigs_file = open('contig.fasta','r')
outLengthsFile = open('metahit_contig_lengths.txt','w')

lengths = []
curStr = ''
for line in metahit_contigs_file:
	if line[0] == '>':
		if len(curStr) > 0:
			lengths.append(len(curStr))
		curStr = ''
	else:
		curStr += line.strip()
		
for l in lengths:
	outLengthsFile.write(str(l) + '\n')

metahit_contigs_file.close()
outLengthsFile.close()