function [newModel] = makeSpecificSubsetModel(fullModel,rxnsNames2keep,description)
% Keep only those reactions that contain genes that map to 
% the given bin.
%
% Written by Matt Biggs, May 2015

% Find reactions that are not associated with any genes
spontaneousRxns2keep = logical(zeros(size(fullModel.rxns)));
for i = 1:length(spontaneousRxns2keep)
    if strcmp(fullModel.grRules{i},'Unknown') || ...
       strcmp(fullModel.grRules{i},'GAP FILLING') || ...
       strcmp(fullModel.grRules{i},'INITIAL GAP FILLING') || ...
       strcmp(fullModel.grRules{i},'UNIVERSAL') || ...
       strcmp(fullModel.grRules{i},'SPONTANEOUS')
        spontaneousRxns2keep(i) = 1;
    end
end

% Identify indices for reactions to keep
rxns2keep = ismember(fullModel.rxns,rxnsNames2keep);
rxns2keep = rxns2keep | spontaneousRxns2keep;

% Remove reactions
newModel = fullModel;
newModel.S = fullModel.S(:, rxns2keep );
newModel.rxns = fullModel.rxns( rxns2keep );
newModel.rxnNames = fullModel.rxnNames( rxns2keep );
newModel.subSystems = fullModel.subSystems( rxns2keep );
newModel.rev = fullModel.rev( rxns2keep );
newModel.lb = fullModel.lb( rxns2keep );
newModel.ub = fullModel.ub( rxns2keep );
newModel.c = fullModel.c( rxns2keep );
newModel.rxnGeneMat = fullModel.rxnGeneMat( rxns2keep, : );
newModel.rules = fullModel.rules( rxns2keep );
newModel.grRules = fullModel.grRules( rxns2keep );
newModel.confidenceScores = fullModel.confidenceScores( rxns2keep );
newModel.rxnECNumbers = fullModel.rxnECNumbers( rxns2keep );
newModel.rxnReferences = fullModel.rxnReferences( rxns2keep );
newModel.ReactionsWithMetNames = fullModel.ReactionsWithMetNames( rxns2keep );
newModel.KEGGSubsys = fullModel.KEGGSubsys( rxns2keep );
newModel.proteins = fullModel.proteins( rxns2keep );
newModel.description = description;

end