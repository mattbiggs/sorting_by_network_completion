function [newModel] = makeBinSpecificModel(fullModel,pegsInCurBin,curBin)
% Keep only those reactions that contain genes that map to 
% the given bin.
%
% Written by Matt Biggs, May 2015

% Find reactions that are in the current bin
genes2keep = ismember(fullModel.genes,pegsInCurBin);
rxns2keep = sum(fullModel.rxnGeneMat(:,genes2keep),2) > 0;

% Remove reactions
newModel = fullModel;
newModel.S = fullModel.S(:, rxns2keep );
newModel.rxns = fullModel.rxns( rxns2keep );
newModel.rxnNames = fullModel.rxnNames( rxns2keep );
newModel.subSystems = fullModel.subSystems( rxns2keep );
newModel.rev = fullModel.rev( rxns2keep );
newModel.lb = fullModel.lb( rxns2keep );
newModel.ub = fullModel.ub( rxns2keep );
newModel.c = fullModel.c( rxns2keep );
newModel.rxnGeneMat = fullModel.rxnGeneMat( rxns2keep, : );
newModel.rules = fullModel.rules( rxns2keep );
newModel.grRules = fullModel.grRules( rxns2keep );
newModel.confidenceScores = fullModel.confidenceScores( rxns2keep );
newModel.rxnECNumbers = fullModel.rxnECNumbers( rxns2keep );
newModel.rxnReferences = fullModel.rxnReferences( rxns2keep );
newModel.ReactionsWithMetNames = fullModel.ReactionsWithMetNames( rxns2keep );
newModel.KEGGSubsys = fullModel.KEGGSubsys( rxns2keep );
newModel.proteins = fullModel.proteins( rxns2keep );
newModel.description = ['Bin ' num2str(curBin)];

end