# README #

Data and code to support the following paper:

Biggs MB and Papin JA (2015). Metabolic Network-Guided Binning of Metagenomic Sequence Fragments.

A web guide to all the data and code is provided at:
http://mattbiggs.bitbucket.org/SoNeC/index.html

### Who do I talk to? ###

Feel free to send questions to Matt Biggs (mb3ad [at] virginia [dot] edu).