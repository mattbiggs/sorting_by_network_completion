% Sorting Fragments from Metagenomic Data
% Based on Metabolic Network Completeness
%
% Written by Matt Biggs, April 2015

load species_model_list_100
reps = 1000;

% Standard Experiment
'Standard Experiment'
dir = '~\Cluster_By_Network_Completeness\Assignment_accuracy_full_SEED_models\';
cluster_by_network_completeness_linked(species_models_100,reps,dir);

% Effect of number of models
'Effect of the Number of Models'
dir = '~\Cluster_By_Network_Completeness\Assignment_accuracy_num_of_models\';
cluster_by_network_completeness_effect_of_num_models(species_models_100, reps, dir);

% Effect of "shadow models"
'Effect of Shadow Models'
dir = '~\Cluster_By_Network_Completeness\Assignment_accuracy_shadow_models\';
cluster_by_network_completeness_shadow_models(species_models_100,reps,dir);

% Pathway enrichment and specific examples where algorithm works & doesn't work
'Enrichment and Specific Examples'
dir = '~\Cluster_By_Network_Completeness\Enrichment_and_Examples\';
cluster_by_network_completeness_pathway_enrichment(species_models_100,reps,dir);

% Reaction essentiality predictions before and after SoNeC
'Reaction essentiality overlap'
dir = '~\Cluster_By_Network_Completeness\Reaction_essentiality\';
cluster_by_network_completeness_rxn_essentiality_screen(species_models_100, 50, dir);