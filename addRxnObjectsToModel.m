function [reformattedSmallModel] = addRxnObjectsToModel(smallModel,reactionObjectList)
% Takes as input a COBRA format model and a list of reaction objects.
% Returns a new model with updated metabolite list and the added
% reactions.
%
% Written by Matt Biggs, June 2015

reformattedSmallModel = smallModel;

% New Metabolite list
newMetabolites = smallModel.mets(:);
newMetaboliteNames = smallModel.metNames(:);
for i = 1:length(reactionObjectList)
    newMetabolites = [newMetabolites(:) ; reactionObjectList{i}.mets(:) ];
    newMetaboliteNames = [newMetaboliteNames(:) ; reactionObjectList{i}.metNames(:) ];
end
[newMetabolites,IA] = unique(newMetabolites);
newMetaboliteNames = newMetaboliteNames(IA);

% Convert small model to new metabolite list
reformattedSmallModel.mets = newMetabolites;
reformattedSmallModel.metNames = newMetaboliteNames;
reformattedSmallModel.b = zeros(size(newMetabolites));
reformattedSmallModel.S = sparse( zeros( length(newMetabolites), length(smallModel.rxns)) );
for i = 1:length(smallModel.mets)
    metIndexInNewList = ismember(newMetabolites,smallModel.mets{i});
    reformattedSmallModel.S(metIndexInNewList,:) = smallModel.S(i,:);
end

% Add reactions from reactionObjectList
for i = 1:length(reactionObjectList)
    curRxnObject = reactionObjectList{i};
    for j = 1:length(curRxnObject.mets)
        metIndexInNewList = ismember(newMetabolites,curRxnObject.mets{j});
        reformattedSmallModel.S(metIndexInNewList,length(reformattedSmallModel.rxns)+1) = curRxnObject.S(j,1);
    end
    reformattedSmallModel.rxns = [reformattedSmallModel.rxns(:); {curRxnObject.rxns}];
    reformattedSmallModel.rxnNames = [reformattedSmallModel.rxnNames(:); {curRxnObject.rxnNames}];
    reformattedSmallModel.subSystems = [reformattedSmallModel.subSystems(:); {''}];
    if curRxnObject.rev(:) == 1
        reformattedSmallModel.lb = [reformattedSmallModel.lb(:); -1000];
    else
        reformattedSmallModel.lb = [reformattedSmallModel.lb(:); 0];
    end
    reformattedSmallModel.ub = [reformattedSmallModel.ub(:); 1000];
    reformattedSmallModel.rev = [reformattedSmallModel.rev(:); curRxnObject.rev(:)];
    reformattedSmallModel.c = [reformattedSmallModel.c(:); 0];
    reformattedSmallModel.rxnGeneMat = [reformattedSmallModel.rxnGeneMat; zeros(size(reformattedSmallModel.rxnGeneMat(1,:)))];
    reformattedSmallModel.rules = [reformattedSmallModel.rules(:); {''}];
    reformattedSmallModel.grRules = [reformattedSmallModel.grRules(:); {''}];
    reformattedSmallModel.confidenceScores = [reformattedSmallModel.confidenceScores(:); {''}];
    reformattedSmallModel.rxnECNumbers = [reformattedSmallModel.rxnECNumbers(:); curRxnObject.rxnECNumbers(:)];
end


end