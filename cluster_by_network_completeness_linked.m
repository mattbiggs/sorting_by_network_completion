function [] = cluster_by_network_completeness_linked(species_models,reps,dir)
% Assign orphan reactions to models based on gaps/dead-end metabolites
% 
% Written by Matt Biggs, April 2015

numModels = 10;
rxnGroupSizeRange = [1 5:5:40];
colors = {'yellow','magenta','cyan','red','green','blue','black'};
allClassificationAccuracyMats = cell(0,1);
allNumRxnsRemoved = cell(0,1);

for rxnGroupSize = rxnGroupSizeRange
    rxnGroupSize
    allFitnessScores = [];
    allROCScores = [];
    rxnsRemoved = zeros(0,1);
    for fractionRxns2remove = 0.05:0.05:0.5 % Check up to 50% removal of model size
        fractionRxns2remove
        totalFitScores = zeros(1,numModels);
        totalROCScores = zeros(1,6);

        for r = 1:reps
            % ---------------------------------------------------------------------------
            % Remove random subset of reactions from each model, record "model-of-origin"
            % ---------------------------------------------------------------------------    
            species_models_subset = species_models(randperm(length(species_models),numModels));
            orphanRxnSet = cell(0,1);
            orphanGroupAssignments = [];
            smaller_model_list = cell(size(species_models_subset));

            maxGroupID = 0;
            for i = 1:numModels
                % Remove appropriate fraction of model rxns
                numRxns2remove = floor(fractionRxns2remove*length(species_models_subset{i}.rxns));
                if numRxns2remove < rxnGroupSize
                    numRxns2remove = rxnGroupSize;
                end
                
                [newSmallerModel, rxnObjects, groupAssignments] = removeNreactions(species_models_subset{i},i,numRxns2remove,rxnGroupSize,maxGroupID);
                maxGroupID = max(groupAssignments);
                smaller_model_list{i} = newSmallerModel;
                orphanRxnSet = [orphanRxnSet ; rxnObjects];
                orphanGroupAssignments = [orphanGroupAssignments; groupAssignments];
            end

            % ---------------------------------------------------------------------------
            % Identify dead-end metabolites in models (root-no-production and
            % root-no-consumption).
            % ---------------------------------------------------------------------------
            for i = 1:numModels
               [notConsumed,notProduced] = findDeadEndMets(smaller_model_list{i});
               smaller_model_list{i}.notProduced = notProduced;
               smaller_model_list{i}.notConsumed = notConsumed;
            end

            % ---------------------------------------------------------------------------
            % Identify overlap in dead-end metabolites (in models) and
            % substrates/products (in reaction set)
            % ---------------------------------------------------------------------------
            [fitScore,group,correctClassification] = calcSingleGapFillingScore(smaller_model_list,orphanRxnSet,orphanGroupAssignments,rxnGroupSize);
            fitScoreMax = rowiseMax(fitScore);        
            rocMatrix = calculateROCmetrics(fitScoreMax,correctClassification,[]);
            totalFitScores = totalFitScores + fitScoreMax;
            totalROCScores = totalROCScores + rocMatrix;
        end

        allFitnessScores(end+1,:) = totalFitScores/reps;
        allROCScores(end+1,:) = totalROCScores;
        rxnsRemoved(end+1) = fractionRxns2remove;
    end
    
    % ---------------------------------------------------------------------------
    % Check assignment accuracy
    % ---------------------------------------------------------------------------
    classificationMat = allROCScores/reps; % Rows correspond to the number of removed reactions, columns are TP,FP,TA,FA,TTO,FTO
    allClassificationAccuracyMats{end+1} = classificationMat;
    allNumRxnsRemoved{end+1} = rxnsRemoved;
    f2 = figure(2);
    plots = [];    
    hold on;
    for i = 1:6
        plots(i) = plot(rxnsRemoved, classificationMat(:,i),'color',colors{i});
    end
    hold off;
    legend(plots,{'TP','FP','TA','FA','TTO','FTO'},'Location','eastoutside');
    xlabel('Fraction of Rxns Removed from Models');
    ylim([-0.01 1.01]);
        
    % Save Data
    filenameD = [dir 'rxn_removal_accuracy_data_group_size_' num2str(rxnGroupSize)];
    save(filenameD,'allFitnessScores','allROCScores','rxnsRemoved','reps','classificationMat');
    filename = [dir 'rxn_removal_accuracy_curves_group_size_' num2str(rxnGroupSize)];
    print(f2,filename,'-djpeg');
    dlmwrite([filenameD '_reps_ ' num2str(reps) '.tsv'],[rxnsRemoved(:) classificationMat],'\t');
    close(f2);
end

filenameC = [dir 'classification_accuracy_mats'];
save(filenameC,'allClassificationAccuracyMats','rxnGroupSizeRange','allNumRxnsRemoved');

% ---------------------------------------------------------------------------
% Plot Accuracy as a function of rxn group size
% ---------------------------------------------------------------------------
for rr = 1:3:length(rxnsRemoved)
    classificationMatByGroupSize = zeros(length(rxnGroupSizeRange),6);
    for j = 1:length(rxnGroupSizeRange)    
        % Build classification matrix by group size
        curMat = allClassificationAccuracyMats{j};
        classificationMatByGroupSize(j,:) = curMat(rr,:);
    end
    
    % Plot
    f3 = figure(3);
    plots = [];
    hold on;
    for i = 1:6
        plots(i) = plot(rxnGroupSizeRange, classificationMatByGroupSize(:,i),'color',colors{i});
    end
    hold off;
    legend(plots,{'TP','FP','TA','FA','TTO','FTO'},'Location','eastoutside');
    xlabel('Group Size');
    ylim([-0.01 1.01]);
    
    filenameRF = [dir 'rxn_removal_accuracy_curves_removed_rxns_' strrep(num2str(rxnsRemoved(rr)),'.','p') ];
    print(f3,filenameRF,'-djpeg');
    dlmwrite([filenameRF '_reps_ ' num2str(reps) '.tsv'],[rxnGroupSizeRange(:) classificationMatByGroupSize],'\t');
    close(f3);
end


end


