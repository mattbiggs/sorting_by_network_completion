function [notConsumed,notProduced] = findDeadEndMets(model)

% Duplicate and negate reversible reactions
reversibles = model.S(:,logical(model.rev));
tmpS = [model.S -reversibles];

% Find metabolites that are not consumed or not produced
notConsumedIndices = sum(tmpS < 0,2) == 0;
notProducedIndices = sum(tmpS > 0,2) == 0;

% Ignore completely unused metabolites
unused = notConsumedIndices & notProducedIndices;
notConsumedIndices(unused) = 0;
notProducedIndices(unused) = 0;

% Identify metabolites from lone reversible reactions
nonReversibleS = model.S(:,~logical(model.rev));
singleRevRxnMets = sum(abs(reversibles),2) == 1;
zeroNonRevRxnMets = sum(abs(nonReversibleS),2) == 0;
loneRevRxnMetIndices = singleRevRxnMets & zeroNonRevRxnMets;
notConsumedIndices(loneRevRxnMetIndices) = 1;
notProducedIndices(loneRevRxnMetIndices) = 1;

% Return metabolite lists
notConsumed = model.mets(notConsumedIndices);
notProduced = model.mets(notProducedIndices);

end