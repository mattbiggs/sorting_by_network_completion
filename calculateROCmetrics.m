function [rocMatrix] = calculateROCmetrics(fitScoresMax,correctClassification, models2exclude)
% Calculate true/false positive rates based on the classifications
% from calcGapFillingScores() and rowiseMax(). Leave out a model to
% assess the impact of including reactions from outside species.
%
% Written by Matt Biggs, April 2015

maxima = find(fitScoresMax(1,:));
TP = 0;  % True Positive
FP = 0;  % False Positive
TA = 0;  % True Ambiguous (ambiguous call includes true classification)
FA = 0;  % False Ambiguous (ambiguous call does not include true classification)
TTO = 0; % True Throw Out (if group originates from outside model, correctly ambiguous)
FTO = 0; % False Throw Out (if group does not originate from outside model but is thrown out)

if sum(fitScoresMax) == 0
    if sum(ismember(models2exclude,correctClassification)) > 0
        TTO = 1;
    else
        FTO = 1;
    end
elseif sum(fitScoresMax) == 1
    if maxima == correctClassification
        TP = 1;
    else
        FP = 1;
    end    
elseif sum(fitScoresMax) > 1
    if sum(ismember(maxima,correctClassification)) > 0
        TA = 1;
    else
        FA = 1;
    end
end

rocMatrix = [TP FP TA FA TTO FTO];


end