function [fitScore,group,correctClassification] = calcSingleGapFillingScore(smaller_model_list,orphanRxnSet,groupAssignments,rxnGroupSize)
% Calculate a score (between a single group and each model) that quantifies
% how well the reaction(s) "fit" into a given model (i.e. how many dead-end
% metabolites are fixed). In this version, the model is in COBRA format 
% matlab structure while the orphanRxns are a list of individual reaction
% objects. They are associated with a group in the groupAssignments vector.
%
% Written by Matt Biggs, April 2015

group = randi(max(groupAssignments),1,1);
groupRxnSet = orphanRxnSet(groupAssignments==group);
groupRxnSet = groupRxnSet(1:rxnGroupSize);
correctClassification = groupRxnSet{1}.sourceModel;

fitScores = zeros(rxnGroupSize,length(smaller_model_list));
for r = 1:rxnGroupSize
    for m = 1:length(smaller_model_list)
        rxnSubstratesThatAreDeadEnd = sum(ismember( groupRxnSet{r}.mets(groupRxnSet{r}.S < 0), smaller_model_list{m}.notConsumed ));
        totalRxnSubstrates = sum(groupRxnSet{r}.S < 0);
        rxnProductsThatAreDeadEnd = sum(ismember( groupRxnSet{r}.mets(groupRxnSet{r}.S > 0), smaller_model_list{m}.notProduced ));
        totalRxnProducts = sum(groupRxnSet{r}.S > 0);
        forwardFitScore = rxnSubstratesThatAreDeadEnd/totalRxnSubstrates + rxnProductsThatAreDeadEnd/totalRxnProducts;
        
        % If the reaction is reversible, calculate the score for the
        % reverse direction. Keep the maximum of the forward or reverse
        % scores
        if groupRxnSet{r}.rev == 1
            rxnSubstratesThatAreDeadEnd = sum(ismember( groupRxnSet{r}.mets(groupRxnSet{r}.S > 0), smaller_model_list{m}.notConsumed ));
            totalRxnSubstrates = sum(groupRxnSet{r}.S > 0);
            rxnProductsThatAreDeadEnd = sum(ismember( groupRxnSet{r}.mets(groupRxnSet{r}.S < 0), smaller_model_list{m}.notProduced ));
            totalRxnProducts = sum(groupRxnSet{r}.S < 0);
            reverseFitScore = rxnSubstratesThatAreDeadEnd/totalRxnSubstrates + rxnProductsThatAreDeadEnd/totalRxnProducts;
            fitScores(r,m) = max(forwardFitScore,reverseFitScore);
        else
            fitScores(r,m) = forwardFitScore;
        end
    end
end

% Take the sum of fitness scores within a group and apply to each group
% member
fitScore = sum(fitScores, 1);

end