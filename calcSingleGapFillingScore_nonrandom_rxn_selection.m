function [fitScore] = calcSingleGapFillingScore_nonrandom_rxn_selection(model,groupRxnSet,rxnGroupSize)
% Calculate a score (between a single group and each model) that quantifies
% how well the reaction(s) "fit" into a given model (i.e. how many dead-end
% metabolites are fixed). In this version, the model is in COBRA format 
% matlab structure while the orphanRxnSet is a list of individual reaction
% objects assumed to be from the same group.
%
% Written by Matt Biggs, April 2015

groupRxnSet = groupRxnSet(randperm(length(groupRxnSet),length(groupRxnSet)));
groupRxnSet = groupRxnSet(1:rxnGroupSize);

fitScores = zeros(rxnGroupSize,1);
for r = 1:rxnGroupSize
    rxnSubstratesThatAreDeadEnd = sum(ismember( groupRxnSet{r}.mets(groupRxnSet{r}.S < 0), model.notConsumed ));
    totalRxnSubstrates = sum(groupRxnSet{r}.S < 0);
    rxnProductsThatAreDeadEnd = sum(ismember( groupRxnSet{r}.mets(groupRxnSet{r}.S > 0), model.notProduced ));
    totalRxnProducts = sum(groupRxnSet{r}.S > 0);
    forwardFitScore = rxnSubstratesThatAreDeadEnd/totalRxnSubstrates + rxnProductsThatAreDeadEnd/totalRxnProducts;

    % If the reaction is reversible, calculate the score for the
    % reverse direction. Keep the maximum of the forward or reverse
    % scores
    if groupRxnSet{r}.rev == 1
        rxnSubstratesThatAreDeadEnd = sum(ismember( groupRxnSet{r}.mets(groupRxnSet{r}.S > 0), model.notConsumed ));
        totalRxnSubstrates = sum(groupRxnSet{r}.S > 0);
        rxnProductsThatAreDeadEnd = sum(ismember( groupRxnSet{r}.mets(groupRxnSet{r}.S < 0), model.notProduced ));
        totalRxnProducts = sum(groupRxnSet{r}.S < 0);
        reverseFitScore = rxnSubstratesThatAreDeadEnd/totalRxnSubstrates + rxnProductsThatAreDeadEnd/totalRxnProducts;
        fitScores(r) = max(forwardFitScore,reverseFitScore);
    else
        fitScores(r) = forwardFitScore;
    end
end

% Take the sum of fitness scores within a group and apply to each group
% member
fitScore = sum(fitScores, 1);

end