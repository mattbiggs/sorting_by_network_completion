function [fitScore] = calcGapFillingScore(main_model,groupRxnSet)
% Calculate a score (between a single group and a single model) that quantifies
% how well the reaction(s) "fit" into a given model (i.e. how many dead-end
% metabolites are fixed). In this version, the model and the groupRxnSet
% are both in COBRA format matlab structures.
%
% Written by Matt Biggs, May 2015

rxnGroupSize = length(groupRxnSet.rxns);
fitScores = zeros(rxnGroupSize,1);
for r = 1:rxnGroupSize
    rxnSubstratesThatAreDeadEnd = sum(ismember( groupRxnSet.mets(groupRxnSet.S(:,r) < 0), main_model.notConsumed ));
    totalRxnSubstrates = sum(groupRxnSet.S(:,r) < 0);
    rxnProductsThatAreDeadEnd = sum(ismember( groupRxnSet.mets(groupRxnSet.S(:,r) > 0), main_model.notProduced ));
    totalRxnProducts = sum(groupRxnSet.S(:,r) > 0);
    forwardFitScore = rxnSubstratesThatAreDeadEnd/totalRxnSubstrates + rxnProductsThatAreDeadEnd/totalRxnProducts;

    % If the reaction is reversible, calculate the score for the
    % reverse direction. Keep the maximum of the forward or reverse
    % scores
    if groupRxnSet.rev == 1
        rxnSubstratesThatAreDeadEnd = sum(ismember( groupRxnSet.mets(groupRxnSet.S(:,r) > 0), main_model.notConsumed ));
        totalRxnSubstrates = sum(groupRxnSet.S(:,r) > 0);
        rxnProductsThatAreDeadEnd = sum(ismember( groupRxnSet.mets(groupRxnSet.S(:,r) < 0), main_model.notProduced ));
        totalRxnProducts = sum(groupRxnSet.S(:,r) < 0);
        reverseFitScore = rxnSubstratesThatAreDeadEnd/totalRxnSubstrates + rxnProductsThatAreDeadEnd/totalRxnProducts;
        fitScores(r,1) = max(forwardFitScore,reverseFitScore);
    else
        fitScores(r,1) = forwardFitScore;
    end

end

% Take the sum of fitness scores as the group score
fitScore = sum(fitScores, 1);

end