% Convert SEED models to COBRA models in Matlab
%
% Written by Matt Biggs, April 2015

dirName = 'C:\Users\mb3ad\Box Sync\MATLAB\Cluster_By_Network_Completeness\SEED_model_collection';
listing = dir(dirName);
lname = {listing.name}';
lisdir = logical([listing.isdir]');

% For each species:
listing = listing(~lisdir);
species_models_100 = {};
for i = 1:length(listing)
    if ~isempty(strfind(listing(i).name,'.xls')) && isempty( strfind(listing(i).name,'_CB') )
        % Convert each model to COBRA format
        listing(i).name
        newFileName = reformat_SEED_xls_model([dirName '\' listing(i).name]);
        % Read in each model and store in cell array
        m = d_xls2model_JAB(newFileName);
        m.description = strrep(listing(i).name,'.xls','');
        
        species_models_100{end+1} = m;
    end
end

fileName = [dirName '\species_model_list_100.mat'];
save(fileName,'species_models_100');