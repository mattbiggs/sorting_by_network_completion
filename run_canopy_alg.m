% Group contigs by cross-sample abundances using canopy algorithm
% See "Nature Biotechnology 32, 822�828 (2014) doi:10.1038/nbt.2939" 
%
% Written by Matt Biggs, May 2015

%------------------------------
% Load data
%------------------------------
filename = 'coverage_over_nares_samples.tsv';
fid = fopen(filename, 'r');
headers = textscan(fid, [repmat('%s\t',[1,45]) '%s'], 1);
headers(1) = [];
headers = headers';
fclose(fid);

depth_profiles = dlmread(filename,'\t',1,0);
contigIDs = depth_profiles(:,1);
depth_profiles = depth_profiles(:,2:end);

tetranucleotide_profiles = dlmread('tetranucleotide_frequencies_nares_contigs_K40.tsv','\t',1,0);
tnf_contigIDs = tetranucleotide_profiles(:,1);
tetranucleotide_profiles = tetranucleotide_profiles(:,2:end);

lengths = dlmread('nares_assembly_K40.ContigIndex','\t',2,0);
contigLenIDs = lengths(:,1);
lengths = lengths(:,2);
contigLenIDs(lengths < 42) = [];
lengths(lengths < 42) = [];

% Make sure contig depth profiles are in the same row-wise order as
% the length and tetranucleotide frequencies
[sorted_contigIDs,I] = sort(contigIDs);
depth_profiles = depth_profiles(I,:);

%------------------------------
% Canopy algorithm
%------------------------------

% Reduce to contigs that have non-zero coverage in three or more samples
% and are 800 bp or longer
in3 = sum(depth_profiles > 0,2) >= 3;
gt499 = lengths >= 800;
filter = in3 & gt499;
sum(filter)
sorted_contigIDs_f = sorted_contigIDs(filter);
lengths_f = lengths(filter);
depth_profiles_f = depth_profiles(filter,:);
tetranucleotide_profiles_f = tetranucleotide_profiles(filter,:);

% --------------------------------------------------
% PCA on Tetranucleotide Frequencies
% --------------------------------------------------
tnf_cov = cov(tetranucleotide_profiles_f); 
[V,D] = eigs(tnf_cov,5);
scores = tetranucleotide_profiles_f * V;

eig_vals = diag(D);
cumulative_sum = cumsum(eig_vals) ./ sum(eig_vals)

% --------------------------------------------------
% Canopy algorithm (clustering by Pearson and Spearman correlation)
% --------------------------------------------------
cluster_assignments = 1:sum(filter);
tmp_depth_profiles = [depth_profiles_f scores(:,1:3)]; % Add first PC of tetranucleotide frequency data
numClustersOverTime = [];
lastFewChanges = 10;
threshold2end = 1000;

while(lastFewChanges > 0)
    % Choose random depth profile to merge
    uca = unique(cluster_assignments);
    rca = randsample(uca,1);
    rprof = find(cluster_assignments == rca);
    rprof = rprof(1);
    
    % Correlations
    pcorr = corr(tmp_depth_profiles(rprof,:)',tmp_depth_profiles','type','Pearson');
    scorr = corr(tmp_depth_profiles(rprof,:)',tmp_depth_profiles','type','Spearman');
    
    % Merge clusters if highly correlated
    toMerge = pcorr > 0.93 & scorr > 0.7;
    cluster_assignments(toMerge) = cluster_assignments(rprof);
    tmp_depth_profiles(toMerge,:) = repmat(mean(tmp_depth_profiles(toMerge,:),1),[sum(toMerge) 1]);
        
    numClusters = length(unique(cluster_assignments))
    numClustersOverTime(end+1,1) = numClusters;
    if length(numClustersOverTime) > threshold2end
       lastFewChanges = sum( abs(diff(numClustersOverTime(end-threshold2end:end))) );
    end
    lastFewChanges
end

% Clean up cluster assignment numbers
uca = unique(cluster_assignments(:));
cluster_sizes = zeros(size(uca));
for i = 1:length(uca)
   cluster_assignments(cluster_assignments == uca(i)) = i; 
   cluster_sizes(i,1) = sum(cluster_assignments == i);
end

% Find correlations between all clusters and write to file
final_depth_profiles = zeros(length(uca),size(tmp_depth_profiles,2));
for i = 1:length(uca)
   cluster_index = find(cluster_assignments==i);
   cluster_index = cluster_index(1);
   final_depth_profiles(i,:) = tmp_depth_profiles(cluster_index,:);
   cluster_sizes(i,1) = sum(cluster_assignments == i);
end
pcorr = corr(final_depth_profiles','type','Pearson');
scorr = corr(final_depth_profiles','type','Spearman');
    
% Write contig assignments to file
dlmwrite('contig_assignments.tsv',[sorted_contigIDs_f(:) cluster_assignments(:)],'delimiter','\t','precision',7);

% Write Pearson correlations between clusters to file
dlmwrite('bin_pearson_correlations.tsv',pcorr,'delimiter','\t','precision',7);

% Write Spearman correlations between clusters to file
dlmwrite('bin_spearman_correlations.tsv',scorr,'delimiter','\t','precision',7);


% Read contig assignments from previous clustering run
assigned_clusters = dlmread('C:\Users\mb3ad\Desktop\anterior_nares\contig_assignments_p9_s6.tsv','\t',0,0);
contigIDs = assigned_clusters(:,1);
assigned_clusters = assigned_clusters(:,2);

% Write average cluster depth profiles, maxima, and minima to file
allClusters = unique(assigned_clusters);
cluster_counts = zeros(size(allClusters));
fidmax = fopen('cluster_depth_profiles_maxima.tsv','w');
fidave = fopen('cluster_depth_profiles_average.tsv','w');
fidmin = fopen('cluster_depth_profiles_minima.tsv','w');
fidcc = fopen('cluster_counts.tsv','w');
for i = 1:length(allClusters)
    cluster_i = depth_profiles_f(assigned_clusters == i,:);
    if size(cluster_i,1) > 1
        maxima = max(cluster_i);
        average = mean(cluster_i);
        minima = min(cluster_i);
    else
        maxima = cluster_i;
        average = cluster_i;
        minima = cluster_i;
    end
    
    cluster_counts(i) = size(cluster_i,1);
    
    fprintf(fidmax,[num2str(maxima,'%f\t') '\n']);
    fprintf(fidave,[num2str(average,'%f\t') '\n']);
    fprintf(fidmin,[num2str(minima,'%f\t') '\n']);
    fprintf(fidcc,[num2str(i) '\t' num2str(cluster_counts(i)) '\n']);
end
fclose(fidmax);
fclose(fidave);
fclose(fidmin);
fclose(fidcc);





