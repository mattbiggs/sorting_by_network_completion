function [reformattedModel] = SMILEY_gap_fill(model,biomassMets,hoursLimit)
%-------------------------------------------------------------------------- 
% Implementation of the SMILEY algorithm as presented in Reed et al(2006).
% Uses a universal reaction set derived from the Model Seed Database.
%
% Input:
% model = COBRA model formatted to the same metabolite list as the
%         Universal reaction matrices (use reformatMets.m)
% Output:
% newModel = Gap-filled COBRA-format model
% addFromU = names of reactions to add from Universal rxn set
% addFromUindex = logical index into Universal rxn set (each column
%                 corresponds to the results from one iteration)
% addFromX = names of reactions to add from Exchange rxn set
% addFromXindex = logical index into Exchange rxn set (each column
%                 corresponds to the results from one iteration)
%
% Reed JL, Patel TR, Chen KH, Joyce AR, Applebee MK, Herring CD, 
%   Bui OT, Knight EM, Fong SS, and Palsson BO. (2006). Systems
%   approach to refining genome annotation. PNAS, 103(46):17480-17484.
%
% Written by Matt Biggs, June 2015
%-------------------------------------------------------------------------- 
'Loading universal SEED data'
load seed_smat

%---------------------------------------------------
% Get matrices in correct format
% (i.e. same metabolite lists, duplicate revisible rxns)
%---------------------------------------------------
'reformatting models to have consistent metabolite lists'
[reformattedModel,reformattedSEEDModel] = reformat2universal(model,seed_rxns_mat);

'Adding exchange reactions and biomass function'
reformattedModel = addExchangesAndBiomassFn(reformattedModel,biomassMets);

% Create pool of all possible exchange reactions
S = double(full(reformattedModel.S));
U = double(full(reformattedSEEDModel.S));
X = -eye(length(reformattedSEEDModel.mets));

x_rxns = cell(size(reformattedSEEDModel.mets));
for i = 1:length(reformattedSEEDModel.mets)
    x_rxns{i} = ['Ex_' reformattedSEEDModel.mets{i}];
end

%---------------------------------------------------
% Problem Set-up
%---------------------------------------------------
'Creating problem matrices'
[n_mets,n_Urxns] = size(U);
[~,n_Srxns] = size(S);
n_Xrxns = n_mets;

v = zeros(n_Srxns,1);
y = zeros(n_Urxns,1);
z = zeros(n_Xrxns,1);
a = zeros(n_Urxns,1);
b = zeros(n_Xrxns,1);
v_min = reformattedModel.lb(:);
v_max = reformattedModel.ub(:);
y_min = -1000*reformattedSEEDModel.rev(:);
y_max = 1000*ones(n_Urxns,1);
z_min = zeros(n_Xrxns,1);
z_max = 1000*ones(n_Xrxns,1);

objective = [zeros(1,n_Srxns+n_Urxns+n_Xrxns) ones(1,n_Urxns+n_Xrxns)];

'A'
A =  [S U X zeros(size(U)) zeros(size(X)); ...                  %(1)
     [reformattedModel.c' zeros(1,2*n_Urxns+2*n_Xrxns)]; ...    %(2)
     [eye(n_Srxns) zeros(n_Srxns,2*n_Urxns+2*n_Xrxns) ]; ...    %(3)
     [eye(n_Srxns) zeros(n_Srxns,2*n_Urxns+2*n_Xrxns) ]; ... 
     [zeros(n_Urxns,n_Srxns) eye(n_Urxns) zeros(n_Urxns,n_Xrxns) -1*diag(y_min) zeros(n_Urxns,n_Xrxns)]; ... %(4)
     [zeros(n_Urxns,n_Srxns) eye(n_Urxns) zeros(n_Urxns,n_Xrxns) -1*diag(y_max) zeros(n_Urxns,n_Xrxns)]; ...
     [zeros(n_Xrxns,n_Srxns) zeros(n_Xrxns,n_Urxns) eye(n_Xrxns) zeros(n_Xrxns,n_Urxns) -1*diag(z_min)]; ... %(5)
     [zeros(n_Xrxns,n_Srxns) zeros(n_Xrxns,n_Urxns) eye(n_Xrxns) zeros(n_Xrxns,n_Urxns) -1*diag(z_max)]; ...
     ];
A = sparse(double(A));

'RHS'
RHS = [ zeros(n_mets,1); ... %(1)
        0.05; ...            %(2)
        v_min; ...           %(3)
        v_max; ...
        zeros(size(y_min)); ... %(4)
        zeros(size(y_max)); ...
        zeros(size(z_min)); ... %(5)
        zeros(size(z_max)) ];

'Sense'
sense = [repmat('=',n_mets,1); ... %(1)
         '>'; ... %(2)
         repmat('>',n_Srxns,1); ... %(3)
         repmat('<',n_Srxns,1); ...
         repmat('>',n_Urxns,1); ... %(4)
         repmat('<',n_Urxns,1); ...
         repmat('>',n_Xrxns,1); ... %(5)
         repmat('<',n_Xrxns,1); ...
         ];

vtype = [repmat('C',n_Srxns+n_Urxns+n_Xrxns,1); repmat('B',n_Urxns+n_Xrxns,1);];

size(objective)
size(A)
size(RHS)
size(sense)

%---------------------------------------------------
% Optimization
%---------------------------------------------------
'Solving Problem'
try
    clear gurobi_model;
    gurobi_model.A = A;
    gurobi_model.obj = objective;
    gurobi_model.lb = -1000*ones(size(objective));
    gurobi_model.rhs = RHS;
    gurobi_model.sense = sense;
    gurobi_model.vtype = vtype;
    gurobi_model.modelsense = 'min';
    clear params;
    params.outputflag = 0;
    params.TimeLimit = 60*60*hoursLimit; 
    result = gurobi(gurobi_model, params);
    disp(result)
    
    biomassVal = result.x(find(reformattedModel.c))
    a = result.x(n_Srxns+n_Urxns+n_Xrxns+1 : n_Srxns+n_Urxns+n_Xrxns+n_Urxns);
    b = result.x(n_Srxns+2*n_Urxns+n_Xrxns+1 : n_Srxns+2*n_Urxns+n_Xrxns+n_Xrxns);
    
    addFromUindex = logical(a(:));
    addFromU = reformattedSEEDModel.rxns(addFromUindex);
    Urxns = U(:,addFromUindex);
    addFromXindex = logical(b(:));
    addFromX = x_rxns(addFromXindex);
    Xrxns = X(:,addFromXindex);
    
    % Update gap-filled model
    reformattedModel.S = [ S Urxns Xrxns ];
    reformattedModel.rxns = [ reformattedModel.rxns; addFromU; addFromX ];    
    reformattedModel.rxnNames = [ reformattedModel.rxnNames; reformattedSEEDModel.rxnRoles(addFromUindex); addFromX ];
    reformattedModel.subSystems = [ reformattedModel.subSystems; reformattedSEEDModel.rxnSubsys(addFromUindex); repmat({''},[length(addFromX),1]) ];
    reformattedModel.rxnECNumbers = [ reformattedModel.rxnECNumbers; reformattedSEEDModel.rxnECnums(addFromUindex); repmat({''},[length(addFromX),1]) ];
    reformattedModel.confidenceScores = [ reformattedModel.confidenceScores; repmat({''},[length(addFromU)+length(addFromX),1]) ];
    reformattedModel.rev = double([ reformattedModel.rev; reformattedSEEDModel.rev(addFromUindex); zeros(size(addFromX)) ]);
    reformattedModel.c = [ reformattedModel.c; zeros(size(addFromU)) ; zeros(size(addFromX)) ];
    reformattedModel.lb = double([ reformattedModel.lb; y_min(addFromUindex); zeros(size(addFromX)) ]);
    reformattedModel.ub = [ reformattedModel.ub; y_max(addFromUindex); z_max(addFromXindex) ];
    reformattedModel.rxnGeneMat = [reformattedModel.rxnGeneMat; zeros(length(addFromU) + length(addFromX), size(reformattedModel.rxnGeneMat,2))];
    reformattedModel.rules = [ reformattedModel.rules; repmat({''},[length(addFromU)+length(addFromX),1])];
    reformattedModel.grRules = [ reformattedModel.grRules; repmat({''},[length(addFromU)+length(addFromX),1])];
    
catch gurobiError
    gurobiError
    fprintf('Gurobi error reported\n');
end

'Finished'

end

