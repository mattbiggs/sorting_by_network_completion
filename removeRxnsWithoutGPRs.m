function [newModel] = removeRxnsWithoutGPRs(model)
% Specifically for SEED reconstructions, remove all reactions
% that do not have a specific gene in the GPR (i.e. that 
% have 'Unknown' in the place of a gene list).
%
% Written by Matt Biggs, April 2015

% Find reactions that are to be removed
rxns2keep = ~strcmp('Unknown',model.grRules);

% Remove reactions
newModel = model;
newModel.S = model.S(:, rxns2keep );
newModel.rxns = model.rxns( rxns2keep );
newModel.rxnNames = model.rxnNames( rxns2keep );
newModel.rev = model.rev( rxns2keep );
newModel.lb = model.lb( rxns2keep );
newModel.ub = model.ub( rxns2keep );
newModel.c = model.c( rxns2keep );
newModel.rxnGeneMat = model.rxnGeneMat( rxns2keep, : );
newModel.rules = model.rules( rxns2keep );
newModel.grRules = model.grRules( rxns2keep );
newModel.confidenceScores = model.confidenceScores( rxns2keep );
newModel.rxnECNumbers = model.rxnECNumbers( rxns2keep );
newModel.rxnReferences = model.rxnReferences( rxns2keep );
newModel.ReactionsWithMetNames = model.ReactionsWithMetNames( rxns2keep );
newModel.KEGGSubsys = model.KEGGSubsys( rxns2keep );
newModel.proteins = model.proteins( rxns2keep );


end