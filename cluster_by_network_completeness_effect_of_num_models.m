function [] = cluster_by_network_completeness_effect_of_num_models(species_models, reps, dir)
% Assign orphan reactions to models based on gaps/dead-end metabolites
% species_models is a cell array of COBRA-format metabolic models.
% 
% Written by Matt Biggs, April 2015

allClassificationAccuracyMats = [];
numberOfModels = [2:9 10:10:length(species_models)];

for numMods = numberOfModels
    rxnGroupSize = 25;       
    fractionRxns2remove = 0.15;
    totalROCScores = zeros(1,6);

    for r = 1:reps
        if mod(r,50) == 0
           r 
        end
        % ---------------------------------------------------------------------------
        % Remove random subset of reactions from each model, record "model-of-origin"
        % ---------------------------------------------------------------------------    
        species_subset = species_models( randperm(length(species_models),numMods) );
        orphanRxnSet = cell(0,1);
        orphanGroupAssignments = [];
        smaller_model_list = cell(size(species_subset));

        maxGroupID = 0;
        for i = 1:length(species_subset)
            % Remove appropriate fraction of model
                numRxns2remove = floor(fractionRxns2remove*length(species_subset{i}.rxns));
                if numRxns2remove < rxnGroupSize
                    numRxns2remove = rxnGroupSize;
                end
            [newSmallerModel, rxnObjects, groupAssignments] = removeNreactions(species_subset{i},i,numRxns2remove,rxnGroupSize,maxGroupID);
            maxGroupID = max(groupAssignments);
            smaller_model_list{i} = newSmallerModel;
            orphanRxnSet = [orphanRxnSet ; rxnObjects];
            orphanGroupAssignments = [orphanGroupAssignments; groupAssignments];
        end

        % ---------------------------------------------------------------------------
        % Identify dead-end metabolites in models (root-no-production and
        % root-no-consumption).
        % ---------------------------------------------------------------------------
        for i = 1:length(species_subset)
           [notConsumed,notProduced] = findDeadEndMets(smaller_model_list{i});
           smaller_model_list{i}.notProduced = notProduced;
           smaller_model_list{i}.notConsumed = notConsumed;
        end

        % ---------------------------------------------------------------------------
        % Identify overlap in dead-end metabolites (in models) and
        % substrates/products (in reaction set)
        % ---------------------------------------------------------------------------
        % calcSingleGapFillingScore() randomly selects a single group from the orphanRxnSet and calculuates the score for that group        
        [fitScore,group,correctClassification] = calcSingleGapFillingScore(smaller_model_list,orphanRxnSet,orphanGroupAssignments,rxnGroupSize);
        fitScoreMax = rowiseMax(fitScore);        
        rocMatrix = calculateROCmetrics(fitScoreMax,correctClassification,[]);
        totalROCScores = totalROCScores + rocMatrix;
    end
    
    % ---------------------------------------------------------------------------
    % Check assignment accuracy
    % ---------------------------------------------------------------------------
    classificationMat = totalROCScores/reps; % Rows correspond to the number of removed reactions, columns are TP,FP,TA,FA,TTO,FTO
    allClassificationAccuracyMats(end+1,:) = classificationMat;

end

filenameC = [dir 'num_models_classification_accuracy_mats'];
save(filenameC,'allClassificationAccuracyMats','numberOfModels');

% ---------------------------------------------------------------------------
% Plot Accuracy as a function of the number of models
% ---------------------------------------------------------------------------
f1 = figure(1);
plots = [];
colors = {'yellow','magenta','cyan','red','green','blue','black'};
hold on;
for i = 1:6
    plots(i) = plot(numberOfModels, allClassificationAccuracyMats(:,i),'color',colors{i});
end
hold off;
legend(plots,{'TP','FP','TA','FA','TTO','FTO'},'Location','eastoutside');
xlabel('Number of Models');
ylim([-0.01 1.01]);

filenameNM = [dir 'num_models_rxn_removal_accuracy'];
print(f1,filenameNM,'-djpeg');
close(f1);

% Write to a file that can be read into R for plotting
filename = [dir 'num_models_classification_accuracies_reps_' num2str(reps) '.tsv'];
dlmwrite(filename,[numberOfModels(:) allClassificationAccuracyMats],'\t');

end

