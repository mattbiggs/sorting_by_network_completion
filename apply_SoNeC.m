% Apply SoNeC algorithm to data from Anterior Nares.
%
% Written by Matt Biggs, May 2015

% ---------------------------------------------------------------------------
% Read Data
% ---------------------------------------------------------------------------

% Read in SEED model and convert to COBRA format
newFileName = reformat_SEED_xls_model('C:\Users\mb3ad\Desktop\anterior_nares\nares_K40_p9_s6_reconstruction.xls');
newFileName = 'nares_K40_p9_s6_reconstruction_CB.xls';
m = d_xls2model_JAB(newFileName);
m.description = 'Anterior Nares Bins K40, Pearson 0.9, and Spearman 0.6';
        
% Read in bin co-correlations
pcorr = dlmread('bin_pearson_correlations_p9_s6.tsv');
scorr = dlmread('bin_spearman_correlations_p9_s6.tsv');

% Map pegs to contigs
filename = 'contigs2pegs.results.out';
fid = fopen(filename, 'r');
pegs2contigs = textscan(fid, '%s %d %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s');
p2c_pegs = pegs2contigs{1};
for i = 1:length(p2c_pegs)
    p2c_pegs{i} = strrep(p2c_pegs{i},'fig|6666666.118848.','');
end
p2c_contigs = pegs2contigs{2};
fclose(fid);

% Read contig bin assignments
filename = 'contig_assignments_p9_s6.tsv';
fid = fopen(filename, 'r');
contigBinAssignments = textscan(fid, '%d %d');
cba_contigs = contigBinAssignments{1};
cba_bins = contigBinAssignments{2};
uBins = unique(cba_bins);
fclose(fid);

% ---------------------------------------------------------------------------
% Separate SEED model into canopy algorithm bins
% ---------------------------------------------------------------------------
binModels = cell(size(uBins(:)));
numRxnsInbins = zeros(size(uBins(:)));
for i = 1:length(uBins)
    curBin = uBins(i);
    
    % Determine pegs that map to current bin
    contigsInBin = cba_contigs(cba_bins == curBin);
    contigMap = ismember(p2c_contigs,contigsInBin);
    pegsInCurBin = p2c_pegs(contigMap);
    
    % Reduce full model to only the reactions corresponding to the current bin
    binModels{i} = makeBinSpecificModel(m,pegsInCurBin,curBin);
    numRxnsInbins(i) = length(binModels{i}.rxns);
end

[sortedBinRxnNum,I] = sort(numRxnsInbins,'descend');

% ---------------------------------------------------------------------------
% Choose main bins
% ---------------------------------------------------------------------------
mainBins = [614;1357];
newPcorr = pcorr;
newScorr = scorr;
newPcorr(:,mainBins) = zeros(size(newPcorr(:,mainBins)));
newScorr(:,mainBins) = zeros(size(newScorr(:,mainBins)));

% ---------------------------------------------------------------------------
% Choose bins:
%   1) With enough reactions 
%   2) Closely correlated with two main bins
% ---------------------------------------------------------------------------
ambiguousContigs = sum(newPcorr(mainBins,:) > 0.2,1) >= 1;
binsWenoughRxns = numRxnsInbins >= 25;
SoNeCcandidates = find(ambiguousContigs' & binsWenoughRxns);
SoNeCcandidates = [752; 2653; 1608; 595; 2836; 2475; 145]; % Annotated as Staphylococcus bins w/ >= 10 rxns
size(SoNeCcandidates)

% ---------------------------------------------------------------------------
% Use the SoNeC algorithm to assign those smaller bins to the larger ones
% ---------------------------------------------------------------------------

% Identify dead-end metabolites in models (root-no-production and
% root-no-consumption).
for i = 1:length(mainBins)
    mainBin_i = mainBins(i);
   [notConsumed,notProduced] = findDeadEndMets(binModels{mainBin_i});
   binModels{mainBin_i}.notProduced = notProduced;
   binModels{mainBin_i}.notConsumed = notConsumed;
end

% Identify overlap in dead-end metabolites (in models) and
% substrates/products (in reaction set)
scores = zeros(length(SoNeCcandidates),length(mainBins));
for sc = 1:length(SoNeCcandidates)
    scandidate = SoNeCcandidates(sc);
    for mb = 1:length(mainBins)
        mbin = mainBins(mb);
        [fitScore] = calcGapFillingScore(binModels{mbin},binModels{scandidate});
        scores(sc,mb) = fitScore;
    end    
end
scores
scoreMaxima = rowiseMax(scores)       

% Assignments
assignmentResultsFile = fopen('assignmentResults.tsv','w');
for sc = 1:length(SoNeCcandidates)
    scandidate = SoNeCcandidates(sc);
    destinationBins = find(scoreMaxima(sc,:));
    destinationBinStrings = {};
    for j = 1:length(destinationBins)
        destinationBinStrings{j} = num2str(mainBins(destinationBins(j)));
    end
    fprintf(assignmentResultsFile,[num2str(scandidate) '\t' num2str(strjoin(destinationBinStrings,',')) '\n']);       
end
fclose(assignmentResultsFile);

% ---------------------------------------------------------------------------
% Compare reaction overlap between S. aureus bin and full S. aureus
% reconstruction
% ---------------------------------------------------------------------------
msa = d_xls2model_JAB('SaureusN315_CB.xls');
msa.description = 'S. aureus N315';
load SEEDrxns2KEGGmaps

% Bin 614 consistently "BLASTs" well to S. aureus
assigned2staph = [ 1608; 145 ]; % Lengths = 1152 bp and 1816 bp
initialRxnOverlap = sum(ismember(msa.rxns,binModels{614}.rxns))
combined614RxnList = binModels{614}.rxns;
for i = 1:length(assigned2staph)
     combined614RxnList = unique([combined614RxnList; binModels{assigned2staph(i)}.rxns]);
end
rxnOverlap614AfterSoNeC = sum(ismember(msa.rxns,combined614RxnList))

% Identify unique reactions in 614
b4rxnOverlap = ismember(binModels{614}.rxns,msa.rxns);
b4uniqueRxns = ~b4rxnOverlap;
sum(b4rxnOverlap)
sum(b4uniqueRxns)
afterRxnOverlap = ismember(combined614RxnList,msa.rxns);
afterUniqueRxns = ~afterRxnOverlap;
sum(afterRxnOverlap)
sum(afterUniqueRxns)
unique614Subsystems = unique(SEEDrxns2KEGGmaps.KeggMaps(ismember(SEEDrxns2KEGGmaps.Reaction,binModels{614}.rxns(b4uniqueRxns))));
unique614Subsystems

% Bin 1357 consistently "BLASTs" well to S. aureus
assigned2staph = [ 2653; 595; 2836 ]; % Lengths = 2512 bp, 59378 bp and 8023 bp
initialRxnOverlap = sum(ismember(msa.rxns,binModels{1357}.rxns))
combined1357RxnList = binModels{1357}.rxns;
for i = 1:length(assigned2staph)
     combined1357RxnList = unique([combined1357RxnList; binModels{assigned2staph(i)}.rxns]);
end
rxnOverlap1357AfterSoNeC = sum(ismember(msa.rxns,combined1357RxnList))

% Identify unique reactions in 1357
b4rxnOverlap = ismember(binModels{1357}.rxns,msa.rxns);
b4uniqueRxns = ~b4rxnOverlap;
sum(b4rxnOverlap)
sum(b4uniqueRxns)
afterRxnOverlap = ismember(combined1357RxnList,msa.rxns);
afterUniqueRxns = ~afterRxnOverlap;
sum(afterRxnOverlap)
sum(afterUniqueRxns)
unique1357Subsystems = unique(SEEDrxns2KEGGmaps.KeggMaps(ismember(SEEDrxns2KEGGmaps.Reaction,binModels{614}.rxns(b4uniqueRxns))));
unique1357Subsystems

% ---------------------------------------------------------------------------
% Find rare subsystems in anterior nares reconstruction
% ---------------------------------------------------------------------------
subsysList = SEEDrxns2KEGGmaps.KeggMaps(ismember(SEEDrxns2KEGGmaps.Reaction,m.rxns));
ANsubsys = cell(0,1);
for i = 1:length(subsysList)
    subsystems = strsplit(subsysList{i},'|');
    for j = 1:length(subsystems)
        ANsubsys{end+1,1} = subsystems{j};
    end
end
uniqueANsubsys = unique(ANsubsys);

binRxnList = {};
for i = 1:length(binModels)
   binRxnList = [binRxnList; binModels{i}.rxns]; 
end

binsubsysList = SEEDrxns2KEGGmaps.KeggMaps(ismember(SEEDrxns2KEGGmaps.Reaction,binRxnList));
BINsubsys = cell(0,1);
for i = 1:length(binsubsysList)
    subsystems = strsplit(binsubsysList{i},'|');
    for j = 1:length(subsystems)
        BINsubsys{end+1,1} = subsystems{j};
    end
end

counts = zeros(size(uniqueANsubsys));
for i = 1:length(uniqueANsubsys)
    counts(i) = sum(strfind_cellarray(BINsubsys,uniqueANsubsys{i}));
end

[sorted_counts,I] = sort(counts);
sortedUniqueANsubsys = uniqueANsubsys(I);

% ---------------------------------------------------------------------------
% Subsystem counts before and after 
% ---------------------------------------------------------------------------

% 614 before
bin614b_subsysList = SEEDrxns2KEGGmaps.KeggMaps(ismember(SEEDrxns2KEGGmaps.Reaction,binModels{614}.rxns));
Bin614b_subsys = cell(0,1);
for i = 1:length(bin614b_subsysList)
    subsystems = strsplit(bin614b_subsysList{i},'|');
    for j = 1:length(subsystems)
        Bin614b_subsys{end+1,1} = subsystems{j};
    end
end
counts614b = zeros(size(uniqueANsubsys));
for i = 1:length(uniqueANsubsys)
    counts614b(i) = sum(strfind_cellarray(Bin614b_subsys,uniqueANsubsys{i}));
end

% 614 after
bin614a_subsysList = SEEDrxns2KEGGmaps.KeggMaps(ismember(SEEDrxns2KEGGmaps.Reaction,combined614RxnList));
Bin614a_subsys = cell(0,1);
for i = 1:length(bin614a_subsysList)
    subsystems = strsplit(bin614a_subsysList{i},'|');
    for j = 1:length(subsystems)
        Bin614a_subsys{end+1,1} = subsystems{j};
    end
end
counts614a = zeros(size(uniqueANsubsys));
for i = 1:length(uniqueANsubsys)
    counts614a(i) = sum(strfind_cellarray(Bin614a_subsys,uniqueANsubsys{i}));
end

% 1357 before
bin1357b_subsysList = SEEDrxns2KEGGmaps.KeggMaps(ismember(SEEDrxns2KEGGmaps.Reaction,binModels{1357}.rxns));
Bin1357b_subsys = cell(0,1);
for i = 1:length(bin1357b_subsysList)
    subsystems = strsplit(bin1357b_subsysList{i},'|');
    for j = 1:length(subsystems)
        Bin1357b_subsys{end+1,1} = subsystems{j};
    end
end
counts1357b = zeros(size(uniqueANsubsys));
for i = 1:length(uniqueANsubsys)
    counts1357b(i) = sum(strfind_cellarray(Bin1357b_subsys,uniqueANsubsys{i}));
end

% 1357 before
bin1357a_subsysList = SEEDrxns2KEGGmaps.KeggMaps(ismember(SEEDrxns2KEGGmaps.Reaction,combined1357RxnList));
Bin1357a_subsys = cell(0,1);
for i = 1:length(bin1357a_subsysList)
    subsystems = strsplit(bin1357a_subsysList{i},'|');
    for j = 1:length(subsystems)
        Bin1357a_subsys{end+1,1} = subsystems{j};
    end
end
counts1357a = zeros(size(uniqueANsubsys));
for i = 1:length(uniqueANsubsys)
    counts1357a(i) = sum(strfind_cellarray(Bin1357a_subsys,uniqueANsubsys{i}));
end

% S. aureus reference model
binSa_subsysList = SEEDrxns2KEGGmaps.KeggMaps(ismember(SEEDrxns2KEGGmaps.Reaction,msa.rxns));
BinSa_subsys = cell(0,1);
for i = 1:length(binSa_subsysList)
    subsystems = strsplit(binSa_subsysList{i},'|');
    for j = 1:length(subsystems)
        BinSa_subsys{end+1,1} = subsystems{j};
    end
end
countsSa = zeros(size(uniqueANsubsys));
for i = 1:length(uniqueANsubsys)
    countsSa(i) = sum(strfind_cellarray(BinSa_subsys,uniqueANsubsys{i}));
end

% Identify interesting subsystems
ignore = find((counts614a + counts614b + counts1357a + counts1357b  + countsSa) == 0);
ignore = unique([ignore; find(counts614a == counts614b & counts1357a == counts1357b)]);
M = [counts614b counts614a counts1357b counts1357a countsSa]
  
stacked_grouped_bar_graph_data = [];
for i = 1:length(counts614b)
    if sum(ignore==i) == 0
        stacked_grouped_bar_graph_data = [stacked_grouped_bar_graph_data [counts614b(i) 0 0 ; counts614a(i)-counts614b(i) 0 0; 0 counts1357b(i) 0; 0 counts1357a(i)-counts1357b(i) 0; 0 0 countsSa(i)] ];
    end
end

dlmwrite('stacked_grouped_graph_data.tsv',stacked_grouped_bar_graph_data,'\t');

fid = fopen('interesting_subsystems.tsv','w');
for i = 1:length(uniqueANsubsys)
    if sum(ignore==i) == 0
        fprintf(fid,[uniqueANsubsys{i} '\n']);
    end
end
fclose(fid);

fid = fopen('subsystems_before_and_after_binning.tsv','w');
fprintf(fid,'SubsystemNames\t614before\t614after\t1357before\t1357after\tSaureusReference\n');
for i = 1:length(uniqueANsubsys)
    fprintf(fid,[uniqueANsubsys{i} '\t' num2str(counts614b(i)) '\t' num2str(counts614a(i)) '\t' num2str(counts1357b(i)) '\t' num2str(counts1357a(i)) '\t' num2str(countsSa(i)) '\n']);
end
fclose(fid);

% ---------------------------------------------------------------------------
% Rxn Essentiality Screen
% ---------------------------------------------------------------------------
% Biomass components from BMC Microbiology 2005, 5:8  doi:10.1186/1471-2180-5-8 (http://www.biomedcentral.com/1471-2180/5/8/)
initCobraToolbox()
biomassMets = {'cpd00023'; 'cpd00053'; 'cpd00035'; 'cpd00039'; 'cpd00107'; 'cpd00041'; 'cpd00132'; 'cpd00156'; 'cpd00322'; 'cpd00051'; 'cpd00033'; ...
               'cpd00066'; 'cpd00054'; 'cpd00161'; 'cpd00069'; 'cpd00129'; 'cpd00060'; 'cpd00119'; 'cpd00065'; 'cpd00381'; 'cpd00006'; 'cpd00002'; ...
               'cpd00003'; 'cpd00015'; 'cpd00126'; 'cpd00018'; 'cpd00091'; 'cpd00046'; 'cpd00773'; 'cpd00294'; 'cpd00298'; 'cpd00296'; 'cpd00206'; ...
               'cpd00557'; 'cpd11313'; 'cpd00504'; 'cpd02611'; 'cpd00122'; 'cpd03671'; 'cpd00087'; 'cpd00264'; 'cpd00214'; 'cpd00118'};
newmsa = SMILEY_gap_fill(msa, biomassMets,1)
cpmsa2 = canProduceMets( newmsa,biomassMets )

% Collect biomass component names
biomassMetNames = cell(size(biomassMets));
for i = 1:length(biomassMets)
    biomassMetNames{i} = seed_rxns_mat.metNames(ismember(seed_rxns_mat.mets,[biomassMets{i}]));    
end
biomassMetNames{cpmsa2 == 0}

% Bin 614 consistently "BLASTs" well to S. aureus
assigned2staph = [ 1608; 145 ]; % Lengths = 1152 bp and 1816 bp
combinedRxnList = binModels{614}.rxns;
for i = 1:length(assigned2staph)
     combinedRxnList = unique([combinedRxnList; binModels{assigned2staph(i)}.rxns]);
end

% Build and gapfill 614 models (before and after SoNeC)
m614b = makeSpecificSubsetModel(m,binModels{614}.rxns,'Model for 614 before SoNeC');
m614b_gf = SMILEY_gap_fill(m614b,biomassMets,1)
cp614b = canProduceMets( m614b_gf,biomassMets )
m614a = makeSpecificSubsetModel(m,combinedRxnList,'Model for 614 after SoNeC');
m614a_gf = SMILEY_gap_fill(m614a,biomassMets,1)
cp614a2 = canProduceMets( m614a_gf,biomassMets )

% Check reaction essentiality in 614 before and after SoNeC (only check
% rxns associated with genes)

% load m614ab

% Remove biomass components that can't be produced
canProduce = biomassMets(cp614a2 > 0.01 & cp614b > 0.01);
m614b_gf.S(:,find(m614b_gf.c)) = 0;
m614a_gf.S(:,find(m614a_gf.c)) = 0;
for i = 1:length(canProduce)
    curMet = [canProduce{i} '[c]'];
    m614b_gf.S(ismember(m614b_gf.mets,curMet),find(m614b_gf.c)) = -1;
    m614a_gf.S(ismember(m614a_gf.mets,curMet),find(m614a_gf.c)) = -1;
end

% Staph aureus reference
unknown_index_ref = ismember(newmsa.genes,'Unknown');
rxnsWithGenes_indices_ref = find(sum(newmsa.rxnGeneMat(:,~unknown_index_ref),2));
essentialRxns_ref = zeros( size(rxnsWithGenes_indices_ref) );
for i = 1:length(essentialRxns_ref)
    [num2str(i) ' of ' num2str(length(essentialRxns_ref))]
    ref_tmp = newmsa;
    ref_tmp.lb(rxnsWithGenes_indices_ref(i)) = 0;
    ref_tmp.ub(rxnsWithGenes_indices_ref(i)) = 0;
    sol = optimizeCbModel(ref_tmp);
    essentialRxns_ref(i) = sol.f;
end
essential_ref = newmsa.rxns(rxnsWithGenes_indices_ref(essentialRxns_ref==0));
genes_ref = unique(newmsa.genes(find(sum(newmsa.rxnGeneMat(rxnsWithGenes_indices_ref(essentialRxns_ref==0),:),1))));

% Before
unknown_index_b = ismember(m614b_gf.genes,'Unknown');
rxnsWithGenes_indices_before = find(sum(m614b_gf.rxnGeneMat(:,~unknown_index_b),2));
essentialRxns_b = zeros( size(rxnsWithGenes_indices_before) );
for i = 1:length(essentialRxns_b)
    [num2str(i) ' of ' num2str(length(essentialRxns_b))]
    m614b_gf_tmp = m614b_gf;
    m614b_gf_tmp.lb(rxnsWithGenes_indices_before(i)) = 0;
    m614b_gf_tmp.ub(rxnsWithGenes_indices_before(i)) = 0;
    sol = optimizeCbModel(m614b_gf_tmp);
    essentialRxns_b(i) = sol.f;
end
essential_b = m614b_gf.rxns(rxnsWithGenes_indices_before(essentialRxns_b==0));
genes_b = unique(m614b_gf.genes(find(sum(m614b_gf.rxnGeneMat(rxnsWithGenes_indices_before(essentialRxns_b==0),:),1))));

% After
unknown_index_a = ismember(m614a_gf.genes,'Unknown');
rxnsWithGenes_indices_after = find(sum(m614a_gf.rxnGeneMat(:,~unknown_index_a),2));
essentialRxns_a = zeros( size(rxnsWithGenes_indices_after) );
for i = 1:length(essentialRxns_a)
    [num2str(i) ' of ' num2str(length(essentialRxns_a))]
    m614a_gf_tmp = m614a_gf;
    m614a_gf_tmp.lb(rxnsWithGenes_indices_after(i)) = 0;
    m614a_gf_tmp.ub(rxnsWithGenes_indices_after(i)) = 0;
    sol = optimizeCbModel(m614a_gf_tmp);
    essentialRxns_a(i) = sol.f;
end
essential_a = m614a_gf.rxns(rxnsWithGenes_indices_after(essentialRxns_a==0));
genes_a = unique(m614a_gf.genes(find(sum(m614a_gf.rxnGeneMat(rxnsWithGenes_indices_after(essentialRxns_a==0),:),1))));

% Identify uniquely-essential reactions before and after SoNeC
addedBySoNeC = m614a.rxns(~ismember(m614a.rxns,m614b.rxns));
unique_b = essential_b(~ismember(essential_b,essential_a));
shared = essential_a(ismember(essential_a,essential_b));
unique_a = essential_a(~ismember(essential_a,essential_b));

unique_ref = essential_ref(~ismember(shared,essential_ref));
shared_ref_b = essential_ref(ismember(essential_ref,essential_b));
shared_ref_a = essential_ref(ismember(essential_ref,essential_a));

% Display essential reactions using MetDraw
