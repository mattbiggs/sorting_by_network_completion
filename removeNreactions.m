function [newSmallerModel, rxnObjects, groupAssignments] = removeNreactions(model,modelID,numRxns2remove,rxnGroupSize,maxGroupID)
% Remove a random subset of reactions from the COBRA-format model
% Return the model with the subset removed AND the removed rxns
%
% Written by Matt Biggs, April 2015

[~,rxns] = size(model.S);

rxns2remove = randperm(rxns,numRxns2remove);

rxnSmat = model.S(:,rxns2remove);

% Remove reactions to make smaller model
newSmallerModel = model;
newSmallerModel.S = model.S(:, ~ismember(1:rxns,rxns2remove) );
newSmallerModel.rxns = model.rxns( ~ismember(1:rxns,rxns2remove) );
newSmallerModel.rxnNames = model.rxnNames( ~ismember(1:rxns,rxns2remove) );
newSmallerModel.rev = model.rev( ~ismember(1:rxns,rxns2remove) );
newSmallerModel.lb = model.lb( ~ismember(1:rxns,rxns2remove) );
newSmallerModel.ub = model.ub( ~ismember(1:rxns,rxns2remove) );
newSmallerModel.c = model.c( ~ismember(1:rxns,rxns2remove) );
newSmallerModel.rxnGeneMat = model.rxnGeneMat( ~ismember(1:rxns,rxns2remove), : );
newSmallerModel.rules = model.rules( ~ismember(1:rxns,rxns2remove) );
newSmallerModel.grRules = model.grRules( ~ismember(1:rxns,rxns2remove) );
newSmallerModel.confidenceScores = model.confidenceScores( ~ismember(1:rxns,rxns2remove) );
newSmallerModel.rxnECNumbers = model.rxnECNumbers( ~ismember(1:rxns,rxns2remove) );
newSmallerModel.rxnReferences = model.rxnReferences( ~ismember(1:rxns,rxns2remove) );
newSmallerModel.ReactionsWithMetNames = model.ReactionsWithMetNames( ~ismember(1:rxns,rxns2remove) );
newSmallerModel.KEGGSubsys = model.KEGGSubsys( ~ismember(1:rxns,rxns2remove) );
newSmallerModel.proteins = model.proteins( ~ismember(1:rxns,rxns2remove) );

% Assign reactions to groups (simulates genes from the same "contig") of size 'rxnGroupSize'
% Determine groups
if rxnGroupSize <= numRxns2remove
    numGroups = floor(numRxns2remove / rxnGroupSize);
    numRxnsLeftover = numRxns2remove - numGroups*rxnGroupSize;
    groupAssignments = [];
    for i = 1:numGroups
        groupAssignments = [groupAssignments; i*ones(rxnGroupSize,1)];
    end
    groupAssignments = [groupAssignments; randi(numGroups,[numRxnsLeftover,1])];
    groupAssignments = groupAssignments + maxGroupID;
else
    groupAssignments = 1:numRxns2remove;
end

% Make reaction objects
rxnObjects = cell(numRxns2remove,1);
for i = 1:numRxns2remove
    tmpRxn.sourceModel = modelID;
    rxn2remove = rxns2remove(i);
    tmpRxn.S = model.S(find(model.S(:,rxn2remove)), rxn2remove);
    tmpRxn.mets = model.mets( find(model.S(:,rxn2remove)) );
    tmpRxn.metNames = model.metNames( find(model.S(:,rxn2remove)) );
    tmpRxn.metFormulas = model.metFormulas( find(model.S(:,rxn2remove)) );
    tmpRxn.rxns = model.rxns{ rxn2remove };
    tmpRxn.rxnNames = model.rxnNames{ rxn2remove };
    tmpRxn.rev = model.rev( rxn2remove );
    tmpRxn.confidenceScores = model.confidenceScores{ rxn2remove };
    tmpRxn.rxnECNumbers = model.rxnECNumbers{ rxn2remove };
    tmpRxn.rxnReferences = model.rxnReferences{ rxn2remove };
    tmpRxn.KEGGSubsys = model.KEGGSubsys{ rxn2remove };    
    tmpRxn.groupAssignment = groupAssignments(i);
    rxnObjects{i} = tmpRxn;
end


end