Acinetobacter baumannii ATCC 17978
Agrobacterium tumefaciens str. C58
Akkermansia muciniphila ATCC BAA-835
Bacillus subtilis subsp. subtilis str. 168
Barnesiella intestinihominis YIT 11860
Barnesiella viscericola DSM 18177
Blautia hansenii DSM 20583
Blautia hydrogenotrophica DSM 10507
Blautia producta ATCC 27340
Blautia schinkii DSM 10518
Blautia sp. KLE 1732
Blautia wexlerae AGR2146
Buchnera aphidicola str. 5A (Acyrthosiphon pisum)
Burkholderia cepacia R1808
Burkholderia pseudomallei K96243
Burkholderia xenovorans LB400
Butyrivibrio fibrisolvens 16/4
Butyrivibrio sp. LC3010
Caulobacter crescentus CB15
Clostridiales bacterium VE202-06
Clostridium bolteae 90A9
Clostridium clostridioforme 90A1
Clostridium difficile CD37
Clostridium hathewayi DSM 13479
Coprobacillus sp. 3_3_56FAA
Coprobacillus sp. 8_2_54BFAA
Coprobacillus sp. D7
Coprococcus sp. ART55/1
Coxiella burnetii RSA 493
Enterobacter cloacae ISC8
Enterobacter sp. MGH 25
Enterococcus durans ATCC 6056
Enterococcus faecalis TX0411
Enterococcus faecium 505
Enterococcus faecium EnGen0127 strain 2006-70-121
Enterococcus flavescens ATCC 49996
Enterococcus gallinarum EG2
Enterococcus mundtii ATCC 882
Enterococcus raffinosus ATCC 49464
Enterococcus saccharolyticus 30_1
Enterococcus sp. GMD1E
Escherichia coli str. K-12 substr. MG1655
Flavobacterium johnsonia johnsoniae UW101
Fusobacterium nucleatum subsp. nucleatum ATCC 25586
Geobacter metallireducens GS-15
Gluconobacter oxydans 621H
Idiomarina loihiensis L2TR
Klebsiella oxytoca M5al
Klebsiella pneumoniae KCTC 2242
Lachnospira multipara ATCC 19207
Lactobacillus plantarum WCFS1
Lactococcus lactis subsp. lactis Il1403
Listeria innocua Clip11262
Listeria monocytogenes EGD-e
Methylobacillus flagellatus KT
Mycoplasma canis PG 14
Mycoplasma capricolum subsp. capricolum 14232
Mycoplasma gallisepticum NC95_13295-2-2P
Mycoplasma genitalium M2288
Mycoplasma hyopneumoniae 168
Mycoplasma hyorhinis ATCC 17981 strain BTS7
Mycoplasma mycoides subsp. capri PG3
Mycoplasma pneumoniae 19294
Nitrosococcus oceani ATCC 19707
Oribacterium sp. FC2011
Pelagibacter ubique HTCC1062
Peptoclostridium difficile QCD-76w55
Porphyromonas gingivalis W83
Prochlorococcus marinus subsp. pastoris str. CCMP1986
Pseudobutyrivibrio ruminis AD2017
Pseudomonas aeruginosa PAO1
Pseudomonas fluorescens PfO-1
Pseudomonas putida KT2440
Rhizobium leguminosarum bv. viciae 3841
Roseburia intestinalis L1-82
Ruminococcus gnavus ATCC 29149
Ruminococcus obeum A2-162
Ruminococcus torques L2-14
Salinibacter ruber DSM 13855
Salmonella enterica subsp. enterica Serovar Cubana str. CFSAN002050
Serratia marcescens subsp. marcescens ATCC 13880
Shewanella amazonensis SB2B
Shewanella halifaxensis HAW-EB4
Shewanella oneidensis MR-1
Shewanella putrefaciens CN-32
Shewanella woodyi ATCC 51908
Shigella flexneri 2005184
Staphylococcus aureus subsp. aureus JH1
Staphylococcus aureus subsp. aureus Mu3
Staphylococcus aureus subsp. aureus N315
Staphylococcus aureus subsp. aureus str. Newman
Staphylococcus aureus subsp. aureus USA300
Streptococcus pneumoniae R6
Streptococcus pyogenes M1 GAS
Streptococcus thermophilus CNRZ1066
Streptomyces coelicolor A3
Ureaplasma parvum serovar 3 str. ATCC 27815
Ureaplasma urealyticum serovar 10 str. ATCC 33699
Vibrio cholerae O1 biovar eltor str. N16961
Yersinia pestis KIM10+