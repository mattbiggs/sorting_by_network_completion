function [newFileName] = reformat_KBASE_xls_model(fileName)
% Reformats a KBASE-generated metabolic reconstruction in XLS format
% to conform to the COBRA xlsread() function specifications.
%
% Written by Matt Biggs, October 2014

% First, read data
[~,~,cmpnds] = xlsread(fileName,'FBAModelCompounds');
[~,~,rctns] = xlsread(fileName,'FBAModelReactions');

% Second, Re-format data
% Reformat reactions sheet
reactions = cell(0,0);
reactions(:,1) = rctns(:,1);
reactions{1,1} = 'Abbreviation';
reactions(:,2) = rctns(:,5);
reactions{1,2} = 'Name';
for i = 1:length(rctns(:,1))
    reactions(i,1) = strrep(reactions(i,1),'_c0','');
    reactions(i,2) = strrep(reactions(i,2),'_c0','');
end
reactions(:,3) = rctns(:,9);
reactions{1,3} = 'Reaction';
% Add compartments to equation
rev = num2cell(zeros(length(reactions(:,3)),1));
obj = num2cell(zeros(length(reactions(:,3)),1));
objrow = 0;
emptyRxns = [];
mets = cell(0,0);
metCompartments = cell(0,0);
for i = 2:length(reactions(:,3))
    compartment = strrep(rctns{i,3},'0','');
    tmpRxn = reactions{i,3};
    if ~isnan(tmpRxn)
        tmpRxn = strrep(tmpRxn, '(', '');
        tmpRxn = strrep(tmpRxn, ')', '');
        [rps, matches] = strsplit(tmpRxn, {' + ',' <-> ', ' -> ', ' <- '});
        if ismember(' <- ',matches)
           rps = fliplr(rps);
           matches = fliplr(matches);
        end
        for j = 1:length(rps)
            tmpMet = strsplit(rps{j});
            mets{end+1,1} = tmpMet{end}(1:end-4);
            metCompartments{end+1,1} = tmpMet{end};
        end
        tmpRxn2 = rps{1};
        for j = 2:length(rps)
            if strcmp(matches{j-1}, ' <- ') || strcmp(matches{j-1}, ' -> ')
                matches{j-1} = ' -> ';
            end
            if strcmp(matches{j-1}, ' <-> ')
                rev{i} = 1;
            end
            tmpRxn2 = [tmpRxn2 matches{j-1} rps{j}];
        end
        reactions{i,3} = tmpRxn2;
    else
        rev{i} = NaN;
    end
    % Check objective function
    rxnName = rctns{i,5};
    if ~isempty( strfind(rxnName,'Biomass') )
        obj{i} = 1;
        objrow = i;        
    end
    % Remove empty reactions    
    if isnan(rctns{i,3})
        emptyRxns(end+1) = i;
    end
end
reactions(:,4) = num2cell( repmat(NaN, size(reactions(:,1))) );
reactions{1,4} = 'Reactions with MetNames';
reactions(:,5) = rctns(:,4);
reactions{1,5} = 'GPR';
reactions(:,6) = num2cell( repmat(NaN, size(reactions(:,1))) );
reactions{1,6} = 'Genes';
reactions(:,7) = num2cell( repmat(NaN, size(reactions(:,1))) );
reactions{1,7} = 'Protein';
reactions(:,8) = num2cell( repmat(NaN, size(reactions(:,1))) );
reactions{1,8} = 'Subsystems';
reactions(:,9) = num2cell( repmat(NaN, size(reactions(:,1))) );
reactions{1,9} = 'KEGGSubsys';
reactions(:,10) = rev;
reactions{1,10} = 'Reversible';
lb = zeros(size(rev));
for i = 1:length(rev)
    revi = rev{i};
    if revi == 1
        lb(i) = -1000;
    else
        lb(i) = 0;
    end
end
ub = 1000*ones(size(rev));
reactions(:,11) = num2cell(lb);
reactions{1,11} = 'Lower bound';
reactions(:,12) = num2cell(ub);
reactions{1,12} = 'Upper bound';
reactions(:,13) = obj;
reactions{1,13} = 'Objective';
reactions(:,14) = num2cell( repmat(NaN, size(reactions(:,1))) );
reactions{1,14} = 'Confidence Score';
reactions(:,15) = num2cell( repmat(NaN, size(reactions(:,1))) );
reactions{1,15} = 'EC. Number';
reactions(:,16) = num2cell( repmat(NaN, size(reactions(:,1))) );
reactions{1,16} = 'Notes';
reactions(:,17) = num2cell( repmat(NaN, size(reactions(:,1))) );
reactions{1,17} = 'References';

% Remove biomass and empty reactions
if ~isempty(emptyRxns)
    reactions(emptyRxns,:) = [];
elseif objrow ~= 0
    reactions(objrow,:) = [];
end

% Reformat compounds sheet
compounds = cell(0,0);
compounds(:,1) = cmpnds(:,1);
compounds{1,1} = 'Abbreviation';
compounds(:,2) = cmpnds(:,2);
compounds{1,2} = 'Name';
compounds(:,3) = num2cell( repmat(NaN, size(compounds(:,1))) );
compounds{1,3} = 'Formula (neutral)';
compounds(:,4) = cmpnds(:,3);
compounds{1,4} = 'Formula (Charged)';
compounds(:,5) = cmpnds(:,4);
compounds{1,5} = 'Charge';
compounds(:,6) = num2cell( repmat(NaN, size(compounds(:,1))) );
compounds{1,6} = 'Compartment';
compounds(:,7) = num2cell( repmat(NaN, size(compounds(:,1))) );
compounds{1,7} = 'KEGG ID';
compounds(:,8) = num2cell( repmat(NaN, size(compounds(:,1))) );
compounds{1,8} = 'PubChemID';
compounds(:,9) = num2cell( repmat(NaN, size(compounds(:,1))) );
compounds{1,9} = 'CheBI ID';
compounds(:,10) = num2cell( repmat(NaN, size(compounds(:,1))) );
compounds{1,10} = 'InchI String';
compounds(:,11) = num2cell( repmat(NaN, size(compounds(:,1))) );
compounds{1,11} = 'Smiles';
compounds(:,12) = num2cell( repmat(NaN, size(compounds(:,1))) );
compounds{1,12} = 'mTBN';
compounds(:,13) = num2cell( repmat(NaN, size(compounds(:,1))) );
compounds{1,13} = 'mTBN_alt';
compounds(:,14) = num2cell( repmat(NaN, size(compounds(:,1))) );
compounds{1,14} = 'mSim';

[metCompartments,ia,~] = unique(metCompartments);
mets = mets(ia);

% Compartmentalize compounds
len = length(compounds(:,1));
for i = 2:len
   abbrev = compounds{i,1};
   index = ismember(mets,abbrev);
   fi = find(index);
   compounds{i,1} = metCompartments{fi(1)};
   if length(fi) > 1
      for j = 2:length(fi)
          compounds(end+1,:) = compounds(i,:);
          compounds{length(compounds),1} = metCompartments{fi(j)};
      end
   end
end

% Third, Write data
% Write new XLS file
[pathstr,name,ext] = fileparts(fileName);
newFileName = [pathstr '\' name '_CB' ext];
xlswrite(newFileName, compounds, 1);
xlswrite(newFileName, reactions, 2);

% rename sheets
e = actxserver('Excel.Application'); % # open Activex server
ewb = e.Workbooks.Open(newFileName); % # open file (enter full path!)
ewb.Worksheets.Item(1).Name = 'metabolites'; 
ewb.Worksheets.Item(2).Name = 'reactions'; 
ewb.Worksheets.Item(3).Name = 'genes'; 
ewb.Save % # save to the same file
ewb.Close(false)
e.Quit


end