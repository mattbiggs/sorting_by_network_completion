import HTMLParser
import re
import difflib

#------------------------------------------------------------------		
# Classes and functions
#------------------------------------------------------------------
name2cmpdID = {}
cmpdID2formula = {}
cmpdID2namelist = {}
cmpdID2carbonCheck = {}
cmpdID2keggIDs = {}
keys2name2cmpdID = []
uncertainCompounds = {}
usedCmpds = []

cmpdUncertaintyFile = open('compound_uncertainty.txt','w')
cmpdUncertaintyFile.close()

# Strip HTML from string
class MLStripper(HTMLParser.HTMLParser):
	def __init__(self):
		self.reset()
		self.fed = []
	def prefeed(self,d):
		d1 = self.unescape(d)
		self.feed(d1)
	def handle_data(self, d):
		self.fed.append(d)
	def get_fed_data(self):
		return ''.join(self.fed)

# Parse number of carbons in formula
p = re.compile('C\d*')
def howManyCarbons(formula):
	m = p.match(formula)
	if m:
		if len(m.group()[1:]) > 0:
			numCs = int(m.group()[1:])
			return numCs
		else:
			return 1
	else:
		return 0

# get SEED cmpd from name
p3 = re.compile(r'\[\D\]$')
def getSEEDcmpdFromName(cmpdName):
	mls = MLStripper()
	mls.prefeed(cmpdName)
	cn = mls.get_fed_data().strip()
	compartment = p3.search(cn)
	cn = p3.sub('',cn)
	try:
		cmpdID = name2cmpdID[cn]
	except KeyError:
		try:
			newcn = uncertainCompounds[cn]
			cmpdID = name2cmpdID[newcn]
		except KeyError:
			# If the compound is spelled slightly differently, 
			# use difflib to find the most similar SEED cmpd in database
			similarities = []
			for seedMet in keys2name2cmpdID:
				ratio = difflib.SequenceMatcher(None,cn,seedMet).ratio()
				similarities.append(ratio)
			max_index = similarities.index(max(similarities))
			# log uncertain compounds
			cmpdUncertaintyFile = open('compound_uncertainty.txt','a')
			print cn + ' is most similar to: ' + keys2name2cmpdID[max_index]
			cmpdUncertaintyFile.write(cn + ' is most similar to: ' + keys2name2cmpdID[max_index] + '\n')
			cmpdUncertaintyFile.close()
			# Return identified compound ID
			newcn = keys2name2cmpdID[max_index]
			uncertainCompounds[cn] = newcn
			cmpdID = name2cmpdID[newcn]
	if compartment:
		cmpdID += compartment.group()
	usedCmpds.append(cmpdID)
	return cmpdID

# reformat SEED rxn equation to use seed compound names
p2 = re.compile(r'\(\d*\.*\d*\) (?!-)')
def formatSEEDrxnEqtn(equation):
	equation = equation.strip('"')
	eps = equation.split('|')
	newEq = ''
	rev = 0 # 0 = it's already forward; 1 = needs to be reversed; 2 = reversible and needs to be both forward and reversed
	for ep in eps:
		m = p2.search(ep)
		if ep.find(' + ') > -1 or ep.find('=>') > -1 :
			newEq += ep
		elif ep.find('<=') > -1:
			rev = 1
			newEq += ep
		elif ep.find('<=>') > -1:
			rev = 2
			newEq += ep
		elif m:
			newEq += ep
		elif len(ep.strip()) > 0:
			cmpdID = getSEEDcmpdFromName(ep)
			newEq += cmpdID
		else:
			w = 'weird ---------------> ' + ep
	if rev == 1:
		newEqPs = newEq.split('<=')
		newEq = newEqPs[1] + '=>' + newEqPs[0]
	return newEq

# Fix stoichiometric coefficients that lack parantheses
p4 = re.compile(r'\d+\.?\d*')
def fixCoefficients(reaction):
	rps = reaction.split()
	numBars = 0
	newRxn = ''
	yes = 0
	for i in range(len(rps)):
		rp = rps[i]
		m = p4.search(rp)
		if m:
			if m.start() > 0:
				numBars += rp[:m.start()].count('|')
				if numBars % 2 == 0:
					if rp.find('(') == -1 and rp.find(')') == -1:
						rps[i] = rp.replace(m.group(),'(' + m.group() + ')')
				numBars -= rp[0:m.start()].count('|')
			else:
				if numBars % 2 == 0:
					if rp.find('(') == -1 and rp.find(')') == -1:
						rps[i] = rp.replace(m.group(),'(' + m.group() + ')')
		numBars += rp.count('|')
	newRxn = ' '.join(rps)
	return newRxn

# Read in equation with correct stoichiometric coefficients
def readEquation(equation):
	rxnVector = [float(0)]*len(usedCmpds)
	
	rev = 0
	if equation.find('<=>') > -1:
		rev = 1
	
	if rev == 0:
		halves = equation.split('=>')
	else:
		halves = equation.split('<=>')
		
	# substrates
	ep1 = halves[0].split('+')
	for ep in ep1:
		m = p2.search(ep) # look for stoichiometric coefficient
		if m:
			coeff = float(m.group().replace('(','').replace(')',''))
			newep = ep.replace(m.group(),'').strip()
			index = usedCmpds.index(newep)
			rxnVector[index] = -1*coeff
		else:
			newep = ep.strip()
			if len(newep) > 0:
				index = usedCmpds.index(newep)
				rxnVector[index] = -1
	# products
	ep2 = halves[1].split('+')
	for ep in ep2:
		m = p2.search(ep) # look for stoichiometric coefficient
		if m:
			coeff = float(m.group().replace('(','').replace(')',''))
			newep = ep.replace(m.group(),'').strip()
			index = usedCmpds.index(newep)
			rxnVector[index] = abs(coeff)
		else:
			newep = ep.strip()
			if len(newep) > 0:
				index = usedCmpds.index(newep)
				rxnVector[index] = 1
	return [rxnVector, rev]

# Check that reaction is balanced (at least for non-polymeric compounds)
def check_carbon_balance(equation):
	rev = 0
	if equation.find('<=>') > -1:
		rev = 1
	
	if rev == 0:
		halves = equation.split('=>')
	else:
		halves = equation.split('<=>')
		
	# substrates
	ep1 = halves[0].split('+')
	substrateC = 0
	for ep in ep1:
		m = p2.search(ep) # look for stoichiometric coefficient
		if m:
			coeff = float(m.group().replace('(','').replace(')',''))
			newep = ep.replace(m.group(),'').strip()
			substrateC += coeff*cmpdID2carbonCheck[p3.sub('',newep)]
		else:
			newep = ep.strip()
			if len(newep) > 0:
				substrateC += cmpdID2carbonCheck[p3.sub('',newep)]
	# products
	ep2 = halves[1].split('+')
	productC = 0
	for ep in ep2:
		m = p2.search(ep) # look for stoichiometric coefficient
		if m:
			coeff = float(m.group().replace('(','').replace(')',''))
			newep = ep.replace(m.group(),'').strip()
			productC += coeff*cmpdID2carbonCheck[p3.sub('',newep)]
		else:
			newep = ep.strip()
			if len(newep) > 0:
				productC += cmpdID2carbonCheck[p3.sub('',newep)]
	if substrateC == productC:
		return 1
	else:
		return 0
	
# get KEGG map names from URL
def getKEGGmapNames(url):
	kmhps = url.split('<a href=&#39;javascript:addTab(')
	mapNames = []
	for part in kmhps:
		pps = part.split('"')
		if len(pps) >= 6:
			mapNames.append(pps[6])
		elif part.find('None') > -1:
			mapNames.append('None')
	return ';'.join(mapNames)
		
#------------------------------------------------------------------		
# Read compound file and make dictionary
#------------------------------------------------------------------
# NOTE: The raw compound table from SEED had a weird character that could not be 
#       read by the HTML parser. It was something like a '. See cpd15814 for example.
cmpdFile = open('seed_cmpds.tsv','r')

for line in cmpdFile:
	if line.find('Compound	Name	Formula	Mass	KEGG maps	KEGG CID') == -1:
		lps = line.split('\t')
		cid = lps[0]
		# names
		namelist = lps[1].replace(',<br>','\t')
		namelist = namelist.strip('"')
		mls = MLStripper()
		mls.prefeed(namelist)
		snl = mls.get_fed_data()
		cmpdID2namelist[cid] = snl.replace('\t','|')
		snl = snl.split('\t')
		for name in snl:
			name2cmpdID[name] = cid
		# formula	
		formula = lps[2]
		cmpdID2formula[cid] = formula
		cmpdID2carbonCheck[cid] = howManyCarbons(formula)
		# keggIDs
		keggIDs = lps[5]
		cmpdID2keggIDs[cid] = keggIDs
keys2name2cmpdID = name2cmpdID.keys()
cmpdFile.close()

#------------------------------------------------------------------
# Convert rxns from name to compound IDs
#------------------------------------------------------------------
# NOTE: The raw reaction table from SEED had a weird character that could not be 
#       read by the HTML parser. It was something like a '. I used a copy-paste to replace
#	    them all with a normal '.

rxnFile = open('seed_rxns.tsv','r')
rxnOutFile = open('seed_rxns_formatted.tsv','w')
rxnIDlist = []

replacements = [["teichuronic acid (GlcA + GalNac, 45 repeating unit)","teichuronic acid (GlcA+GalNac, 45-repeating unit)"], \
				["trehalose dimycolate (alpha mycolate + methoxymycolate)","trehalose dimycolate (alpha mycolate+methoxymycolate)"], \
				["trehalose dimycolate (alpha mycolate + ketomycolate)","trehalose dimycolate (alpha mycolate+ketomycolate)"], \
				["tetramycolyl hexaarabinoside (tdm3 + tdm4 + tre) (Mtb)","tetramycolyl hexaarabinoside (tdm3+tdm4+tre) (Mtb)"], \
				["tetramycolyl hexaarabinoside (tdm2 + tdm3 + tre) (Mtb)","tetramycolyl hexaarabinoside (tdm2+tdm3+tre) (Mtb)"], \
				["tetramycolyl hexaarabinoside (tdm1 + tdm4 + tre) (Mtb)","tetramycolyl hexaarabinoside (tdm1+tdm4+tre) (Mtb)"], \
				["tetramycolyl hexaarabinoside (tdm1 + tdm3 + tre) (Mtb)","tetramycolyl hexaarabinoside (tdm1+tdm3+tre) (Mtb)"], \
				["tetramycolyl hexaarabinoside (tdm2 + tdm4 + tre) (Mtb)","tetramycolyl hexaarabinoside (tdm2+tdm4+tre) (Mtb)"], \
				["tetramycolyl hexaarabinoside (tdm1 + tdm2 +tre) (Mtb)","tetramycolyl hexaarabinoside (tdm1+tdm2+tre) (Mtb)"], \
				["Pseudomonas aeruginosa LPS core precursor 2 + KDO2-lipidA","Pseudomonas aeruginosa LPS core precursor 2+KDO2-lipidA"], \
				["Pseudomonas aeruginosa LPS core precursor 3 + KDO2-lipidA","Pseudomonas aeruginosa LPS core precursor 3+KDO2-lipidA"], \
				["Pseudomonas aeruginosa LPS core precursor 4 + KDO2-lipidA","Pseudomonas aeruginosa LPS core precursor 4+KDO2-lipidA"], \
				["Pseudomonas aeruginosa LPS core precursor 5 + KDO2-lipidA","Pseudomonas aeruginosa LPS core precursor 5+KDO2-lipidA"], \
				["Pseudomonas aeruginosa LPS core precursor 6 + KDO2-lipidA","Pseudomonas aeruginosa LPS core precursor 6+KDO2-lipidA"], \
				["Pseudomonas aeruginosa LPS core precursor 7 + KDO2-lipidA","Pseudomonas aeruginosa LPS core precursor 7+KDO2-lipidA"], \
				["Pseudomonas aeruginosa LPS core + KDO2-lipidA","Pseudomonas aeruginosa LPS core+KDO2-lipidA"]	]

for line in rxnFile:
	if line.find('Reaction	Name	Equation	Roles	Subsystems	KEGG maps	Enzyme	KEGG RID') == -1 and \
	   line.find('rxn13802') == -1 and \
	   line.find('rxn13914') == -1 and \
	   line.find('rxn07192') == -1 and \
	   line.find('=') > -1:   
		# Replacements to fix metabolites with " + " in the name 
		for replacement in replacements:
			line.replace(replacement[0],replacement[1])
		# Re-format reactions
		lps = line.split('\t')
		rxnID = lps[0]
		rxnIDlist.append(rxnID)
		rxnName = lps[1]
		mls = MLStripper()
		mls.prefeed(rxnName)
		rxnName = mls.get_fed_data()
		equation = lps[2]
		equation = fixCoefficients(equation)
		roles = lps[3].replace('<br><br>','|')
		subsystem = lps[4]
		keggMapHTML = lps[5]
		keggMaps = getKEGGmapNames(keggMapHTML)
		ECnums = lps[6]
		keggRIDs = lps[7]
		# update equations
		updatedEquation = formatSEEDrxnEqtn(equation)
		# print 'rxnID = ' + rxnID 
		# print 'rxnName = ' + rxnName 
		# print 'updatedEquation = ' + updatedEquation 
		# print 'roles = ' + roles 
		# print 'subsystem = ' + subsystem 
		# print 'keggMaps = ' + keggMaps 
		# print 'ECnums = ' + ECnums 
		# print 'keggRIDs = ' + keggRIDs
		rxnOutFile.write(rxnID + '\t' + rxnName + '\t' + updatedEquation + '\t' + roles + '\t' + subsystem + '\t' + keggMaps + '\t' + ECnums + '\t' + keggRIDs)
rxnFile.close()
rxnOutFile.close()

#------------------------------------------------------------------
# Make reaction network based on carbon-containing compounds
#------------------------------------------------------------------
usedCmpds = list(set(usedCmpds))
print len(rxnIDlist)
print len(usedCmpds)

reversibility = []
carbonBalance = []
rxnNames = []
rxnRoles = []
subsystems = []
rxnKeggMaps = []
rxnECnums = []
rxnKeggRIDs = []
rxnOutFile = open('seed_rxns_formatted.tsv','r')
pathMatOutFile = open('complete_path_matrix.tsv','w')

for line in rxnOutFile:
	rxn = [0]*len(usedCmpds)
	lps = line.split('\t')
	rxnID = lps[0].strip('"')
	rxnNames.append(lps[1].strip('"'))
	equation = lps[2].strip('"')	
	rxnRoles.append(lps[3].strip('"'))
	subsystems.append(lps[4].strip('"'))
	rxnKeggMaps.append(lps[5].strip('"'))
	rxnECnums.append(lps[6].strip('"'))
	rxnKeggRIDs.append(lps[7].strip('"'))
	# Parse the reaction equation and create a matrix of substrate-to-product relationships
	rxnNrev = readEquation(equation)
	rxn = rxnNrev[0]
	reversibility.append([rxnID,rxnNrev[1]])
	carbonBalance.append(check_carbon_balance(equation))
	# write to file
	if sum([abs(x) for x in rxn]) > 0: # confirm that reaction is not empty
		writeStr = rxnID + '\t'
		for i in range(len(rxn)):
			x = rxn[i]
			if abs(x) > 0:
				writeStr += '(' + str(i+1) + ',' + str(x) + ')|' # each pair in parentheses contains the index of the coefficient, and the value at that index
		pathMatOutFile.write(writeStr + '\n')

rxnOutFile.close()
pathMatOutFile.close()

# Other output files
cmpdNameOutfile = open('compound_info.tsv','w')
for cmpID in usedCmpds:
	cmp_base_ID = p3.sub('',cmpID)
	cmpdNameOutfile.write(cmpID + '\t' + cmpdID2formula[cmp_base_ID] + '\t' + str(cmpdID2carbonCheck[cmp_base_ID]) + \
	'\t' + cmpdID2namelist[cmp_base_ID] + '\t' + cmpdID2keggIDs[cmp_base_ID])
cmpdNameOutfile.close()


reversibilityOutfile = open('rxn_info.tsv','w')
for i in range(len(reversibility)):
	rev = reversibility[i]
	cb = carbonBalance[i]
	reversibilityOutfile.write(rev[0] + '\t' + str(rev[1]) + '\t' + str(cb) + '\t' + rxnRoles[i] + '\t' + \
	subsystems[i] + '\t' + rxnKeggMaps[i] + '\t' + rxnECnums[i] + '\t' + rxnKeggRIDs[i])
reversibilityOutfile.close()


