% Read in SEED reactions and make a COBRA-format object
% with all reactions and compounds

fid = fopen('compound_info.tsv','r');
cmpds = textscan(fid, '%s%s%d%s%s','Delimiter','\t');
fclose(fid);

fid = fopen('rxn_info.tsv','r');
revs = textscan(fid, '%s%d%d%s%s%s%s%s','Delimiter','\t');
fclose(fid);

fid = fopen('complete_path_matrix.tsv','r');
rxns = textscan(fid, '%s%s','Delimiter','\t');
fclose(fid);

% Creat COBRA-format object
seed_rxns_mat.mets = cmpds{1};
seed_rxns_mat.metFormulas = cmpds{2};
seed_rxns_mat.carbonsInMets = cmpds{3};
seed_rxns_mat.metNames = cmpds{4};
seed_rxns_mat.metKEGGids = cmpds{5};
seed_rxns_mat.rxns = revs{1};
seed_rxns_mat.rev = revs{2};
seed_rxns_mat.carbonBalanced = revs{3};
seed_rxns_mat.rxnRoles = revs{4};
seed_rxns_mat.rxnSubsys = revs{5};
seed_rxns_mat.rxnKEGGmaps = revs{6};
seed_rxns_mat.rxnECnums = revs{7};
seed_rxns_mat.rxnKEGGids = revs{8};
seed_rxns_mat.S = sparse(zeros(length(seed_rxns_mat.mets),length(seed_rxns_mat.rxns)));

% Populate S matrix
for i = 1:length(rxns{2})
    line = rxns{2}{i};
    line = strrep(line, '(', '');
    line = strrep(line, ')', '');
    rps = strsplit(line,'|');
    for j = 1:length(rps)
       curRp = rps{j};
       if length(curRp) > 1
           entry = strsplit(curRp,',');
           compoundIndex = str2num(entry{1});
           coefficient = str2num(entry{2});
           seed_rxns_mat.S(compoundIndex, i) = coefficient;
       end
    end
end

% Manually save Seed S-matrix as .mat file



