function [equalsRowiseMax] = rowiseMax(M)
% Within each row, the maximum is set to 1 (including ties),
% and everything else is set to 0.
%
% Written by Matt Biggs, April 2015

equalsRowiseMax = zeros(size(M));

for i = 1:size(M,1)
    equalsRowiseMax(i,:) = M(i,:) == max(M(i,:));
end    
    
end