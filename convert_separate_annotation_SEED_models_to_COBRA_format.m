% Convert SEED models to COBRA models in Matlab
%
% Written by Matt Biggs, April 2015

dirName = 'C:\Users\mb3ad\Box Sync\MATLAB\Cluster_By_Network_Completeness\GenomeFractions';
listing = dir(dirName);
lname = {listing.name}';
lisdir = logical([listing.isdir]');
  
% For each species:
listing = listing(lisdir);
dirList = listing(3:end);
separate_annotation_species_models = {};
for i = 1:length(dirList) % Species-level folders
    % Full Model
    newDirName = [dirName '\' dirList(i).name];    
    % 95/5
    p5dir = [newDirName '\5percent'];    
    % 75/25
    p25dir = [newDirName '\25percent'];    
    % 50/50
    p50dir = [newDirName '\50percent'];
    subdirList = {newDirName p5dir p25dir p50dir}';
    
    for j = 1:length(subdirList) % Split-level folders (full, 95/5, 75/25, 50/50)
        curDir = subdirList{j};
        subListing = dir(curDir);
        fileNames = {subListing(3:end).name}';
        
        for k = 1:length(fileNames) % Individual files in each folder
            curFile = [curDir '\' fileNames{k}];
            if ~isempty(strfind(curFile,'_reconstruction.xls')) && isempty( strfind(curFile,'_CB') ) 
                % Convert each model to COBRA format
                newFileName = reformat_SEED_xls_model(curFile);
                % Read in each model and store in cell array
                m = d_xls2model_JAB(newFileName);
                m.description = strrep(fileNames{k},'.xls','');
                        
                separate_annotation_species_models{end+1} = m;
            end
        end
    end
end

% Remove rxns from models that are lacking genetic evidence
for i = 1:length(separate_annotation_species_models)
   separate_annotation_species_models{i} =  removeRxnsWithoutGPRs(separate_annotation_species_models{i});
end

fileName = [dirName '\separate_annotation_species_model_list.mat'];
save(fileName,'separate_annotation_species_models');