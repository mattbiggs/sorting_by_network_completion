function [] = cluster_by_network_completeness_shadow_models(species_models,reps,dir)
% Assign orphan reactions to models based on gaps/dead-end metabolites
% 
% Written by Matt Biggs, April 2015

colors = {'yellow','magenta','cyan','red','green','blue','black'};
rxnGroupSize = 25;
fractionRxns2remove = 0.15;
numModels = 10;

allFitnessScores = [];
allROCScores = [];
shadowModels = zeros(0,1);
for numShadowModels = 0:numModels % Check up to 50% removal of model size
    numShadowModels
    totalROCScores = zeros(1,6);

    for r = 1:reps
        % ---------------------------------------------------------------------------
        % Remove random subset of reactions from each model, record "model-of-origin"
        % ---------------------------------------------------------------------------    
        species_models_subset = species_models(randperm(length(species_models),numModels+numShadowModels));
        orphanRxnSet = cell(0,1);
        orphanGroupAssignments = [];
        smaller_model_list = cell(size(species_models_subset));

        maxGroupID = 0;
        for i = 1:length(species_models_subset)
            % Remove appropriate fraction of model
            numRxns2remove = floor(fractionRxns2remove*length(species_models_subset{i}.rxns));
            if numRxns2remove < rxnGroupSize
                numRxns2remove = rxnGroupSize;
            end

            [newSmallerModel, rxnObjects, groupAssignments] = removeNreactions(species_models_subset{i},i,numRxns2remove,rxnGroupSize,maxGroupID);
            maxGroupID = max(groupAssignments);
            smaller_model_list{i} = newSmallerModel;
            orphanRxnSet = [orphanRxnSet ; rxnObjects];
            orphanGroupAssignments = [orphanGroupAssignments; groupAssignments];
        end

        % ---------------------------------------------------------------------------
        % Identify dead-end metabolites in models (root-no-production and
        % root-no-consumption).
        % ---------------------------------------------------------------------------
        for i = 1:length(species_models_subset)
           [notConsumed,notProduced] = findDeadEndMets(smaller_model_list{i});
           smaller_model_list{i}.notProduced = notProduced;
           smaller_model_list{i}.notConsumed = notConsumed;
        end

        % ---------------------------------------------------------------------------
        % Identify overlap in dead-end metabolites (in models) and
        % substrates/products (in reaction set)
        % ---------------------------------------------------------------------------
        [fitScore,group,correctClassification] = calcSingleGapFillingScore(smaller_model_list,orphanRxnSet,orphanGroupAssignments,rxnGroupSize);
        models2exclude = randi(length(species_models_subset),numShadowModels);
        fitScore(:,models2exclude) = zeros(size(fitScore,1),1);
        fitScoreMax = rowiseMax(fitScore);        
        rocMatrix = calculateROCmetrics(fitScoreMax,correctClassification, models2exclude);
        totalROCScores = totalROCScores + rocMatrix;
    end

    allROCScores(end+1,:) = totalROCScores;
    shadowModels(end+1) = numShadowModels;
end

% ---------------------------------------------------------------------------
% Check assignment accuracy
% ---------------------------------------------------------------------------
classificationMat = allROCScores/reps; % Rows correspond to the number of removed reactions, columns are TP,FP,TA,FA,TTO,FTO
fractionAsShadowModels = shadowModels/numModels;

f2 = figure(2);
plots = [];    
hold on;
for i = 1:6
    plots(i) = plot(fractionAsShadowModels, classificationMat(:,i),'color',colors{i});
end
hold off;
legend(plots,{'TP','FP','TA','FA','TTO','FTO'},'Location','eastoutside');
xlabel('Number of Shadow Models as Fraction of Total');
ylim([-0.01 1.01]);

% Save Data
filenameD = [dir 'ShadowM_rxn_removal_accuracy_data_reps' num2str(reps)];
save(filenameD,'allROCScores','shadowModels','fractionAsShadowModels','reps','classificationMat');
filename = [dir 'ShadowM_rxn_removal_accuracy_curves_reps' num2str(reps)];
print(f2,filename,'-djpeg');
dlmwrite([filenameD '.tsv'],[fractionAsShadowModels(:) classificationMat],'\t');
close(f2);


end


