function [reformattedSmallModel,reformattedUniversalModel] = reformat2universal(smallModel,universalModel)
% Takes as input the universal SEED rxn database (in Cobra model format),
% and a smaller model which will be converted to use the same metabolite
% list, and therefore be easily integrated with new reactions from the 
% universal database.
%
% Written by Matt Biggs, June 2015

reformattedUniversalModel = universalModel;

% Add '[c]' notation to universal metabolites to match smallModel
for i = 1:length(universalModel.mets)
    if isempty(strfind(reformattedUniversalModel.mets{i},'['))
        reformattedUniversalModel.mets{i} = [reformattedUniversalModel.mets{i} '[c]'];
    end
end

% Add metabolites unique to small model to universal metabolite list
uIndices = ~ismember(smallModel.mets,reformattedUniversalModel.mets);
uniqueMets2smallModel = smallModel.mets(uIndices);
reformattedUniversalModel.mets = [reformattedUniversalModel.mets; uniqueMets2smallModel];
reformattedUniversalModel.metFormulas = [reformattedUniversalModel.metFormulas;smallModel.metFormulas(uIndices)];
reformattedUniversalModel.carbonsInMets = [reformattedUniversalModel.carbonsInMets;zeros(size(uniqueMets2smallModel))];
reformattedUniversalModel.metNames = [reformattedUniversalModel.metNames; smallModel.metNames(uIndices)];
reformattedUniversalModel.metKEGGids = [reformattedUniversalModel.metKEGGids; repmat({''},size(uniqueMets2smallModel))];
reformattedUniversalModel.S = sparse([reformattedUniversalModel.S; zeros(length(uniqueMets2smallModel), length(reformattedUniversalModel.rxns) )]);

% Convert small model to new metabolite list
reformattedSmallModel.rxns = smallModel.rxns;
reformattedSmallModel.rxnNames = smallModel.rxnNames;
reformattedSmallModel.subSystems = smallModel.subSystems;
reformattedSmallModel.lb = smallModel.lb;
reformattedSmallModel.ub = smallModel.ub;
reformattedSmallModel.rev = smallModel.rev;
reformattedSmallModel.c = smallModel.c;
reformattedSmallModel.rxnGeneMat = smallModel.rxnGeneMat;
reformattedSmallModel.rules = smallModel.rules;
reformattedSmallModel.grRules = smallModel.grRules;
reformattedSmallModel.genes = smallModel.genes;
reformattedSmallModel.confidenceScores = smallModel.confidenceScores;
reformattedSmallModel.rxnECNumbers = smallModel.rxnECNumbers;
reformattedSmallModel.S = sparse(zeros( length(reformattedUniversalModel.mets), length(smallModel.rxns) ));
reformattedSmallModel.mets = reformattedUniversalModel.mets;
reformattedSmallModel.metNames = reformattedUniversalModel.metNames;
reformattedSmallModel.metFormulas = reformattedUniversalModel.metFormulas;
reformattedSmallModel.metKEGGids = reformattedUniversalModel.metKEGGids;
reformattedSmallModel.b = zeros(size(reformattedUniversalModel.mets));
for i = 1:length(smallModel.mets)
    metIndexInNewList = ismember(reformattedSmallModel.mets,smallModel.mets{i});
    reformattedSmallModel.S(metIndexInNewList,:) = smallModel.S(i,:);
end

% Only keep carbon-balanced rxns
carbonBalanced = reformattedUniversalModel.carbonBalanced == 1;
reformattedUniversalModel.S = reformattedUniversalModel.S(:,carbonBalanced);
reformattedUniversalModel.rxns = reformattedUniversalModel.rxns(carbonBalanced);
reformattedUniversalModel.rev = reformattedUniversalModel.rev(carbonBalanced);
reformattedUniversalModel.rxnRoles = reformattedUniversalModel.rxnRoles(carbonBalanced);
reformattedUniversalModel.carbonBalanced = reformattedUniversalModel.carbonBalanced(carbonBalanced);
reformattedUniversalModel.rxnSubsys = reformattedUniversalModel.rxnSubsys(carbonBalanced);
reformattedUniversalModel.rxnKEGGmaps = reformattedUniversalModel.rxnKEGGmaps(carbonBalanced);
reformattedUniversalModel.rxnECnums = reformattedUniversalModel.rxnECnums(carbonBalanced);
reformattedUniversalModel.rxnKEGGids = reformattedUniversalModel.rxnKEGGids(carbonBalanced);

end