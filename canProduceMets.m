function [ canProduceLogical ] = canProduceMets( inputModel,biomassMets )
% Given a model in Cobra format, and a list of metabolites in that model,
% set each metabolite in turn as the objective function and maximize
% production. Return a logical vector with '1' for each metabolite that can
% be produced (positive, non-zero flux).
%
% Written by Matt Biggs, May 2015

canProduceLogical = zeros(size(biomassMets));
biomassFnIndex = find(inputModel.c);
for i = 1:length(biomassMets)
    % Replace biomass funtion with an exchange rxn for the metabolite of interest
    inputModel.S(:,biomassFnIndex) = zeros(size(inputModel.S(:,biomassFnIndex)));
    metaboliteIndex = find(ismember(inputModel.mets,[biomassMets{i} '[c]']));
    
    if length(metaboliteIndex) > 0
        inputModel.S(metaboliteIndex,biomassFnIndex) = -1;
        stmp = optimizeCbModel(inputModel);
        canProduceLogical(i) = stmp.f;
    else
        canProduceLogical(i) = -1;
    end
end


end

