# Count the number of reads in all files 
for dir in SRS*/
do 
	b = $(sed 's/\///' <<< $dir)
	cat $b/* > $b'/'$b'_all_seqs.fastq'
	python ~/Desktop/count_and_reduce.py $b'/'$b'_all_seqs.fastq' count >> read_counts.txt
done

grep 'Number of sequences in the file is:' read_counts.txt | sed "s/Number of sequences in the file is: //" > read_counts_clean.txt
