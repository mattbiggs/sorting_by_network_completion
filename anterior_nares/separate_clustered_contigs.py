# Separate contigs into clusters
#
# Written by Matt Biggs, May 2015

# Read in assignments file
contig_assignments_file = open('contig_assignments_p9_s6.tsv','r')
contig_IDs = []
contig_assignments = []

for line in contig_assignments_file:
	if len(line) > 0:
		contig_ID, contig_assignment = line.strip().split('\t')
		contig_IDs.append(contig_ID)
		contig_assignments.append(contig_assignment)
contig_assignments_file.close()
contig_ID_set = set(contig_IDs)

# print contig_IDs
# print contig_assignments

# Pull out contigs that belong to clusters and write to new FASTA file
infile = open('nares_assembly_K40.contig','r')
outfile = open('nares_assembly_K40_bins.fasta','w')
inKeep = 0
binned_sequence_lengths = [0]*len(set(contig_assignments))
for line in infile:
	# Check that current sequence is a keeper
	if line[0] == '>':
		lps = line.lstrip('>').split()
		id = lps[0]
		fragement_length = int(lps[2])
		if id in contig_ID_set:
			print id
			print contig_IDs.index(id)
			print int(contig_assignments[contig_IDs.index(id)]) - 1
			binned_sequence_lengths[int(contig_assignments[contig_IDs.index(id)])-1] += fragement_length
			inKeep  = 1
		else:
			inKeep = 0
	# If this one's a keeper, write it to the outfile
	if inKeep == 1:
		if line[0] == '>':
			outfile.write('>' + id + ' cluster=' + contig_assignments[contig_IDs.index(id)] + '\n')
		else:
			outfile.write(line)
infile.close()
outfile.close()

# Write bin stats to file
binStatFile = open('nares_assembly_K40_bins_stats.txt','w')
for i in range(1,len(set(contig_assignments))+1):
	binStatFile.write(str(i) + '\t' + str(contig_assignments.count(str(i))) + '\t' + str(binned_sequence_lengths[i-1]) + '\n')
binStatFile.close()