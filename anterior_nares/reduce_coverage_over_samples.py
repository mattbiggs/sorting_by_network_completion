# Parse the coverage_over_samples.tsv file to include only those samples that 
# had 1,000,000. If a sample had less than 1,000,000, then contigs may
# end up being artificially correlated just because everything seems
# less abundant in an undersampled sample.
#
# Written by Matt Biggs, May 2015

sample_read_content_file = open('read_counts.tsv','r')
samples2keep = []
for line in sample_read_content_file:
	lps = line.strip().split()
	id = lps[0]
	numReads = int(lps[1])
	if numReads >= 1000000:
		samples2keep.append(id)
sample_read_content_file.close()

# Reduce the coverage_over_samples file to be much smaller
coverage_over_samples_file = open('coverage_over_samples.tsv','r')
new_coverage_file = open('coverage_over_nares_samples.tsv','w')
for line in coverage_over_samples_file:
	if line.find('ContigID') > -1:
		lps = line.strip().replace('./nares_assembly_K40/','').replace('_coverage.txt','').split()
		cols2keep = [lps.index(x) for x in lps if x in samples2keep]
		new_coverage_file.write('ContigID\t' + '\t'.join(samples2keep) + '\n')
	else:
		lps = line.strip().split()
		id = lps[0]
		coverage = [lps[i] for i in cols2keep]
		new_coverage_file.write(id + '\t' + '\t'.join(coverage) + '\n')
		
print samples2keep
print cols2keep
coverage_over_samples_file.close()
new_coverage_file.close()