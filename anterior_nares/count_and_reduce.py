# Count the number of fragments in a FASTA/FASTQ file
# Reduce number of fragments in a FASTA/FASTQ file 
#
# Written by Matt Biggs, April 2015

import sys
import random
from os import path

# Get input
if sys.argv[0].find('count_and_reduce.py') > -1:
	fni = 1
elif sys.argv[1].find('count_and_reduce.py') > -1:
	fni = 2
else:
	fni = 3
print fni

print sys.argv

if len(sys.argv) < (fni + 2) or len(sys.argv) > (fni + 3):
	print 'USAGE: python count_and_reduce.py <input FASTA or FASTQ file> <\'count\' or \'reduce\'> if \'reduce\' then <final reduced number of sequences>'
	exit(0)
if len(sys.argv) >= (fni + 2):
	fileName = sys.argv[fni]
	countORreduce = sys.argv[fni+1]
if len(sys.argv) >= (fni + 3):
	lastItem = sys.argv[fni+2].strip()
	print lastItem
	try:
		reduceNum = int(lastItem)
	except ValueError:
		try:
			reduceNumFile = open(lastItem,'r')
			fileStr = reduceNumFile.readline()
			reduceNum = fileStr.strip().split('\t')
			reduceNum = [int(x) for x in reduceNum]
			reduceNumFile.close()
		except IOError:
			print 'ERROR: The number of sequences to reduce to needs to be an integer or a valid file name.'
			exit(0)

# Check if counting or reducing	
if countORreduce.find('count') > -1:
	countORreduce = 'count'
else:
	countORreduce = 'reduce'
	
# Detect input type (FASTA or FASTQ)
try:
	infile = open(fileName,'r')
	line = infile.readline()
	if line.find('>') > -1:
		fileType = 'FASTA'
	elif line.find('@') > -1:
		fileType = 'FASTQ'
	else:
		print 'Input file should be FASTA or FASTQ'
		exit(0)
	infile.close()
except IOError:
	print 'Error with input file:' + fileName + ' does not exist.'
	exit(0)

print 'Input file is in ' + fileType + ' format.'	

# Add a prefix to file name, leaving path intact
def addPrefix(path2file,prefix):
	newFileNameWithPrefix = prefix + path.basename(path2file)
	newFileNameWithPrefix = path.join(path.dirname(path2file),newFileNameWithPrefix)
	return(newFileNameWithPrefix)
	
# Count and Reduce
def countSeqs(inFileName):
	infile = open(inFileName,'r')
	seqCount = 0
	if fileType == 'FASTA':
		for line in infile:
			if line[0] == '>':
				seqCount += 1
	else:
		lineCount = 0
		for line in infile:			
			if (lineCount % 4 == 0 or lineCount == 1) and line[0] == '@':
				seqCount += 1
			lineCount += 1
	infile.close()
	return(seqCount)

# Reduce
def reduceSeqs(inFileName,numSeqsInFile,reduce2Num):
	if type(reduce2Num) == type(4):
		# Determine how many to remove, and make a list of random sequences to remove
		num2remove = numSeqsInFile - reduce2Num
		listOfSeqs2remove = random.sample(range(1,numSeqsInFile+1),num2remove)
	elif type(reduce2Num) == type([4]):
		listOfSeqs2remove = reduce2Num
	listOfSeqs2remove = set(listOfSeqs2remove)
	
	infile = open(inFileName,'r')
	outfile = open(addPrefix(fileName,'reduced_'),'w')
	seqCount = 0
	if fileType == 'FASTA':
		inKeep = 0
		for line in infile:
			# Count lines
			if line[0] == '>':
				seqCount += 1
			# Check if current sequence is a keeper
			if seqCount in listOfSeqs2remove:
				inKeep = 0
			else:
				inKeep = 1
			# If this one's a keeper, write it to the outfile
			if inKeep == 1:
				outfile.write(line)
	else:
		lineCount = 0
		for line in infile:			
			if (lineCount % 4 == 0 or lineCount == 1) and line[0] == '@':
				seqCount += 1
			# Check if current sequence is a keeper
			if seqCount in listOfSeqs2remove:
				inKeep = 0
			else:
				inKeep = 1
			# If this one's a keeper, write it to the outfile
			if inKeep == 1:
				outfile.write(line)
			lineCount += 1
	infile.close()
	outfile.close()
	# Write to file the sequence numbers that were removed
	skipSeqFile = open(addPrefix(fileName,'skipped_'), 'w')
	skipSeqFile.write('\t'.join([str(x) for x in listOfSeqs2remove]) + '\n')
	skipSeqFile.close()

################################
#
################################
numSeqs = countSeqs(fileName)
print 'Number of sequences in the file is: ' + str(numSeqs)

if countORreduce.find('count') == -1:
	if type(reduceNum) == type(4) and reduceNum >= numSeqs:
		print 'File only contains ' + str(numSeqs) + ' sequences. No need to reduce.'
		exit(0)
	else:
		reduceSeqs(fileName,numSeqs,reduceNum)
		
		if type(reduceNum) == type(4):
			print 'File reduced to ' + str(reduceNum) + ' sequences.'
		else:
			print 'File reduced to ' + str(numSeqs - len(reduceNum)) + ' sequences.'