# Check bin purity based on blast results
#
# Written by Matt Biggs, May 2015

import sys

blastResultsFileName = sys.argv[1]
contigAssignmentsFileName = sys.argv[2]
outFileName = blastResultsFileName + '_bin_purity_summary.txt'

# Read in taxonomy database (from NCBI Taxonomy ftp)
taxDict = {}
taxNameFile = open('names.dmp','r')
for line in taxNameFile:
	#2	|	Bacteria	|	Bacteria <prokaryote>	|	scientific name	|
	if line.find('scientific name') > -1:
		lps = line.split('\t|\t')
		try:
			genus = lps[1].split()[0]
		except IndexError:
			genus = lps[1]
		taxDict[lps[0]] = genus
taxNameFile.close()

# Read in bin assignments for all contigs
contigDict = {}
contigBins = []
contigAssignmentsFile = open(contigAssignmentsFileName,'r')
for line in contigAssignmentsFile:
	lps = line.strip().split()
	contigDict[lps[0]] = lps[1]
	contigBins.append(int(lps[1]))
contigAssignmentsFile.close()
uniqueBins = list(set(contigBins))

# Read in taxonomy for each contig
binTaxaLists = [[]]*len(uniqueBins)
blastResultsFile = open(blastResultsFileName,'r')
contigsAlreadyRead = []
for line in blastResultsFile:
	#3063808	gi|227452846|gb|CP001601.1|	N/A	548476	N/A	73.09	249	59	6	1	245	1506765	1507009	1e-011	82.4
	lps = line.strip().split()
	contigID = lps[0]	
	if (contigID not in contigsAlreadyRead) and (contigID in contigDict.keys()):
		contigsAlreadyRead.append(contigID)
		contigBinNum = int(contigDict[contigID])
		contigTaxID = lps[3]
		contigTaxID = contigTaxID.split(';')[0]
		taxSciName = taxDict[contigTaxID]
		binTaxaLists[uniqueBins.index(contigBinNum)] = binTaxaLists[uniqueBins.index(contigBinNum)] + [taxSciName]
blastResultsFile.close()

# Write taxonomy content of each bin to file
binTaxaListsFile = open(blastResultsFileName + '_bin_taxa_lists.txt','w')
for btl in binTaxaLists:
	binTaxaListsFile.write('\t'.join(btl) + '\n')
binTaxaListsFile.close()

# Record the most common taxonomy for each bin, and 
# how consistent that taxonomy is for each bin (do 
# most contigs have same taxonomy within a bin?)
mostCommonTaxa = []
percentageOfMostCommonTaxon = []
summaryFile = open(outFileName,'w')
summaryFile.write('binID\tMostCommonTaxon\tFractionOfBinThatIsMostCommonTaxon\tBinSize\n')
for i in range(len(binTaxaLists)):
	taxaList = binTaxaLists[i]
	uniqueTaxa = list(set(taxaList))
	if len(uniqueTaxa) > 0:
		uniqueCounts = []
		for ut in uniqueTaxa:
			uniqueCounts.append(taxaList.count(ut))
		mostCommonTaxonIndex = uniqueCounts.index(max(uniqueCounts))
		mostCommonTaxon = uniqueTaxa[mostCommonTaxonIndex]
		mostCommonTaxa.append(mostCommonTaxon)
		if sum(uniqueCounts) > 0:
			tmpPercentageOfMostCommonTaxon = float(max(uniqueCounts)) / sum(uniqueCounts)
		else:
			tmpPercentageOfMostCommonTaxon = 0
		percentageOfMostCommonTaxon.append( tmpPercentageOfMostCommonTaxon )
		summaryFile.write(str(i + 1) + '\t' + mostCommonTaxon + '\t' + str(tmpPercentageOfMostCommonTaxon) + '\t' + str(len(taxaList)) +'\n')
	else:
		summaryFile.write(str(i + 1) + '\t' + '' + '\t' + str(0) + '\t' + str(len(taxaList)) +'\n')
		
	