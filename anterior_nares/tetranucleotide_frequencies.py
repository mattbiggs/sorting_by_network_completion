# Calculate tetranucleotide frequencies of each contig
# 
# Written by Matt Biggs, May 2015

import sys
from itertools import product

infile_name = sys.argv[1]

# Make list of tetranucleotides
nucleotides = ['A','T','G','C']
tns = product(nucleotides,repeat=4)
tnl = []
for tn in tns:
	tnl.append(''.join(tn))
print tnl
# Pull out contigs that belong to clusters and write to new FASTA file
infile = open(infile_name,'r')
outfile = open('tetranucleotide_frequencies_file.tsv','w')
curSeq = ''
curID = ''
outfile.write('ContigID\t' + '\t'.join(tnl) + '\n')
for line in infile:
	if line[0] == '>':		
		lps = line.lstrip('>').split()		
		# Calculate tetranucleotide frequencies for previous seq before resetting
		if len(curSeq) > 0:
			tnf = [0]*len(tnl)
			numTetraNucleotidesInSeq = len(curSeq)-3
			for tnp in tnl:
				tnf[tnl.index(tnp)] = float(curSeq.count(tnp)) / numTetraNucleotidesInSeq
			outfile.write(curID + '\t' + '\t'.join([str(x) for x in tnf]) + '\n')
		# Reset
		curSeq = ''
		curID = lps[0]
	else:
		curSeq += line.strip().upper()
if len(curSeq) > 0:
	tnf = [0]*len(tnl)
	numTetraNucleotidesInSeq = len(curSeq)-3
	for tnp in tnl:
		tnf[tnl.index(tnp)] = float(curSeq.count(tnp)) / numTetraNucleotidesInSeq
	outfile.write(curID + '\t' + '\t'.join([str(x) for x in tnf]) + '\n')
infile.close()
outfile.close()
