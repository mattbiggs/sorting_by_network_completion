# Parse coverage into .tsv
#
# Written by Matt Biggs, May 2015

import sys

# Get file names for coverage files for each sample
fileNames = sys.argv[1:]
numFiles = len(fileNames)

# Get get depths for each contig across all the samples
depths = []
for fn in fileNames:
	depths4fileN = []
	contigIDs = []
	for line in open(fn,'r'):
		if line.find('Depth:') > -1:
			lps = line.strip().split()
			contigIDs.append(lps[0].replace(':',''))
			depths4fileN.append(lps[3].replace('Depth:','').replace('nan','0'))
	depths.append(depths4fileN)

# Concatenate into a single .tsv file
outfile = open('coverage_over_samples.tsv','w')
outfile.write('ContigID\t' + '\t'.join(fileNames) + '\n')
for i in range(len(contigIDs)):
	outfile.write(contigIDs[i] + '\t' + '\t'.join([x[i] for x in depths]) + '\n')
outfile.close()
