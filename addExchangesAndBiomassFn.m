function [ outputStaphModel ] = addExchangesAndBiomassFn( inputStaphModel, biomassMets )
% Add exchange reactions (set bounds to S. aureus minimal medium)
% BMC Microbiology 2005, 5:8  doi:10.1186/1471-2180-5-8 (http://www.biomedcentral.com/1471-2180/5/8/)
%
% Add small biomass equation.
%
% Written by Matt Biggs, May 2015

% Add biomass function and exchange reactions
exMets = {
        'cpd00027'; ... % Glucose
        'cpd00035'; ... % alanine
        'cpd00033'; ... % Glycine
        'cpd00322'; ... % Isoleucine
        'cpd00051'; ... % arginine
        'cpd00156'; ... % Valine
        'cpd00129'; ... % Proline
        'cpd00367'; ... % Cytidine
        
        'cpd00009'; ... % Phosphate
        'cpd00048'; ... % Sulfate
        'cpd00133'; ... % Nicotinamide
        'cpd10515'; ... % Fe2+
        'cpd10516'; ... % Fe3+
        'cpd00007'; ... % Oxygen
        'cpd00305'      % Thiamin  
        };

lb = [ -10; -10; -10; -10; -10; -10; -10; -10; ...
       -1000; -1000; -1000; -1000; -1000; -1000; -1000];
   
ub = 1000*ones(size(lb));
ub(lb == -10) = 10;

outputStaphModel = inputStaphModel;

% Add exchange reactions
for i = 1:length(exMets)
    metIndex = find(ismember(outputStaphModel.mets,[exMets{i} '[c]']));
    outputStaphModel.S(:,end+1) = zeros(size(outputStaphModel.S(:,end)));
    outputStaphModel.S(metIndex,end) = -1;
    outputStaphModel.rxns{end+1} = ['Ex_' exMets{i}];
    outputStaphModel.rxnNames{end+1} = ['Ex_' exMets{i}];
    outputStaphModel.subSystems{end+1} = '';
    outputStaphModel.rev(end+1) = 1;
    outputStaphModel.lb(end+1) = lb(i);
    outputStaphModel.ub(end+1) = ub(i);
    outputStaphModel.c(end+1) = 0;
    outputStaphModel.rxnGeneMat(end+1,:) = zeros(size(outputStaphModel.rxnGeneMat(end,:)));
    outputStaphModel.rules{end+1} = '';
    outputStaphModel.grRules{end+1} = '';
%     outputStaphModel.confidenceScores{end+1} = '';
    outputStaphModel.rxnECNumbers{end+1} = '';
%     outputStaphModel.rxnReferences{end+1} = '';
%     outputStaphModel.ReactionsWithMetNames{end+1} = '';
%     outputStaphModel.KEGGSubsys{end+1} = '';
%     outputStaphModel.proteins{end+1} = '';
end

% Add biomass reaction
outputStaphModel.S(:,end+1) = zeros(size(outputStaphModel.S(:,end)));
for i = 1:length(biomassMets)
    metIndex = find(ismember(outputStaphModel.mets,[biomassMets{i} '[c]']));    
    outputStaphModel.S(metIndex,end) = -1;
end
outputStaphModel.rxns{end+1} = ['Biomass'];
outputStaphModel.rxnNames{end+1} = ['Biomass'];
outputStaphModel.subSystems{end+1} = '';
outputStaphModel.rev(end+1) = 0;
outputStaphModel.lb(end+1) = 0;
outputStaphModel.ub(end+1) = 1000;
outputStaphModel.c(end+1) = 1;
outputStaphModel.rxnGeneMat(end+1,:) = zeros(size(outputStaphModel.rxnGeneMat(end,:)));
outputStaphModel.rules{end+1} = '';
outputStaphModel.grRules{end+1} = '';
% outputStaphModel.confidenceScores{end+1} = '';
outputStaphModel.rxnECNumbers{end+1} = '';
% outputStaphModel.rxnReferences{end+1} = '';
% outputStaphModel.ReactionsWithMetNames{end+1} = '';
% outputStaphModel.KEGGSubsys{end+1} = '';
% outputStaphModel.proteins{end+1} = '';

end

