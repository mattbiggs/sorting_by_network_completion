function [] = cluster_by_network_completeness_pathway_enrichment(species_models, reps, dir)
% Assign orphan reactions to models based on gaps/dead-end metabolites
% species_models is a cell array of COBRA-format metabolic models.
% 
% Written by Matt Biggs, April 2015

numAnswers = 10;
rxnGroupSize = 1;       
fractionRxns2remove = 0.15;
totalROCScores = zeros(1,6);

success_example.description = 'Successful Assignment';
failure_example.description = 'Incorrect Assignment';
allExamples = cell(reps,1);
for r = 1:reps
    if mod(r,50) == 0
       r 
    end
    % ---------------------------------------------------------------------------
    % Remove random subset of reactions from each model, record "model-of-origin"
    % ---------------------------------------------------------------------------    
    species_subset = species_models( randperm(length(species_models),numAnswers) );
    orphanRxnSet = cell(0,1);
    orphanGroupAssignments = [];
    smaller_model_list = cell(size(species_subset));

    maxGroupID = 0;
    for i = 1:length(species_subset)
        % Remove appropriate fraction of model
        numRxns2remove = floor(fractionRxns2remove*length(species_subset{i}.rxns));
        if numRxns2remove < rxnGroupSize
            numRxns2remove = rxnGroupSize;
        end
        [newSmallerModel, rxnObjects, groupAssignments] = removeNreactions(species_subset{i},i,numRxns2remove,rxnGroupSize,maxGroupID);
        maxGroupID = max(groupAssignments);
        smaller_model_list{i} = newSmallerModel;
        orphanRxnSet = [orphanRxnSet ; rxnObjects];
        orphanGroupAssignments = [orphanGroupAssignments; groupAssignments];
    end

    % ---------------------------------------------------------------------------
    % Identify dead-end metabolites in models (root-no-production and
    % root-no-consumption).
    % ---------------------------------------------------------------------------
    for i = 1:length(species_subset)
       [notConsumed,notProduced] = findDeadEndMets(smaller_model_list{i});
       smaller_model_list{i}.notProduced = notProduced;
       smaller_model_list{i}.notConsumed = notConsumed;
    end

    % ---------------------------------------------------------------------------
    % Identify overlap in dead-end metabolites (in models) and
    % substrates/products (in reaction set)
    % ---------------------------------------------------------------------------
    % calcSingleGapFillingScore() randomly selects a single group from the orphanRxnSet and calculuates the score for that group        
    [fitScore,group,correctClassification] = calcSingleGapFillingScore(smaller_model_list,orphanRxnSet,orphanGroupAssignments,rxnGroupSize);
    fitScoreMax = rowiseMax(fitScore);        
    rocMatrix = calculateROCmetrics(fitScoreMax,correctClassification,[]);
    
    % Save examples
    example.modelList = smaller_model_list;
    example.rxn = orphanRxnSet{group};
    example.fitScores = fitScore;
    example.fitScoreMax = fitScoreMax;
    example.rocMatrix = rocMatrix;
    allExamples{r} = example;
    if rocMatrix(1) == 1
        success_example = example;
    elseif rocMatrix(2) == 1
        failure_example = example;
    end   
      
    totalROCScores = totalROCScores + rocMatrix;
end

% Write relevant information to file
fid = fopen([dir 'specific_examples_success_failure.txt'],'w');
fprintf(fid,['Successful Example' '\n']);
models2str = cell(1,length(success_example.modelList)); 
for r = 1:length(success_example.modelList)
    models2str{r} = success_example.modelList{r}.description;
end
fprintf(fid,[strjoin(models2str,',') '\n']);
for r = 1:length(success_example.modelList)
    fprintf(fid,['NotProduced:' strjoin(success_example.modelList{r}.notProduced',',') '\t']);
    fprintf(fid,['NotConsumed:' strjoin(success_example.modelList{r}.notConsumed',',') '\n']);
end
fprintf(fid,['RxnID:' success_example.rxn.rxns '\n']);
fprintf(fid,['fitScores:' num2str(success_example.fitScores) '\n']);
fprintf(fid,['fitScoreMax:' num2str(success_example.fitScoreMax) '\n']);
fprintf(fid,['rocMatrix:' num2str(success_example.rocMatrix) '\n']);

fprintf(fid,['Failure Example' '\n']);
models2str = cell(1,length(failure_example.modelList)); 
for r = 1:length(failure_example.modelList)
    models2str{r} = failure_example.modelList{r}.description;
end
fprintf(fid,[strjoin(models2str,',') '\n']);
for r = 1:length(failure_example.modelList)
    fprintf(fid,['NotProduced:' strjoin(failure_example.modelList{r}.notProduced',',') '\t']);
    fprintf(fid,['NotConsumed:' strjoin(failure_example.modelList{r}.notConsumed',',') '\n']);
end
fprintf(fid,['RxnID:' failure_example.rxn.rxns '\n']);
fprintf(fid,['SourceModel:' num2str(failure_example.rxn.sourceModel) '\n']);
fprintf(fid,['fitScores:' num2str(failure_example.fitScores) '\n']);
fprintf(fid,['fitScoreMax:' num2str(failure_example.fitScoreMax) '\n']);
fprintf(fid,['rocMatrix:' num2str(failure_example.rocMatrix) '\n']);
fclose(fid);

%---------------------------------------------
% Enrichment analysis for the 6 accuracy 
% classifications (TP,FP,TA,FA,TTO,FTO)
%---------------------------------------------

load SEEDrxns2KEGGmaps

numAnswers = length(allExamples);

% Pool all reactions and count subsystems
rxns = cell(0,1);
for i = 1:numAnswers
   rxns = [rxns(:); allExamples{i}.rxn.rxns]; 
end

subsys = cell(0,1);
for i = 1:length(rxns)
   index = find(strfind_cellarray(SEEDrxns2KEGGmaps.Reaction,rxns{i}));
   subsys = [subsys(:); SEEDrxns2KEGGmaps.KeggMaps(index)];
end

allSubsystems = cell(0,1);
for i = 1:length(subsys)
    curSubsystems = strsplit(subsys{i},'|');
    allSubsystems = [allSubsystems(:); curSubsystems(:)]; 
end
usubsys = unique(allSubsystems);
totalSubsysPool = length(allSubsystems);

subsysCounts = zeros(size(usubsys));
for i = 1:length(subsysCounts)
    count = sum(strfind_cellarray(allSubsystems,usubsys{i}));
    subsysCounts(i) = count;
end


%---------------------------------------------
% Check enrichment in each category (e.g. TP, FP, etc.)
%---------------------------------------------
allPvals = zeros(length(usubsys),length(totalROCScores));
for j = 1:length(totalROCScores)
    % Pool reactions within each category
    ROCrxns = cell(0,1);
    categorySize = 0;
    for i = 1:length(allExamples)
       if find(allExamples{i}.rocMatrix) == j
            ROCrxns = [ROCrxns(:); allExamples{i}.rxn.rxns]; 
            categorySize = categorySize + 1;
       end
    end

    ROCsubsys = cell(0,1);
    for i = 1:length(ROCrxns)
       index = find(strfind_cellarray(rxns,ROCrxns{i}));
       ROCsubsys = [ROCsubsys(:); subsys(index)];
    end

    allROCSubsystems = cell(0,1);
    for i = 1:length(ROCsubsys)
        curSubsystems = strsplit(ROCsubsys{i},'|');
        allROCSubsystems = [allROCSubsystems(:); curSubsystems(:)]; 
    end
    ROCtotalSubsysPool = length(allROCSubsystems);

    ROCsubsysCounts = zeros(size(usubsys));
    for i = 1:length(usubsys)
        count = sum(strfind_cellarray(allROCSubsystems,usubsys{i}));
        ROCsubsysCounts(i) = count;
    end

    % Calculate p-values for each category
    ROCpvals = zeros(size(usubsys));
    for i = 1:length(usubsys)        
        numOfSubsys_i_InOverallPopulation = subsysCounts(i);
        % use "totalSubsysPool"
        numOfSubsys_i_InROCpopulation = ROCsubsysCounts(i);
        % use "ROCtotalSubsysPool"
        ROCpvals(i) = sum( hygepdf(numOfSubsys_i_InROCpopulation:numOfSubsys_i_InOverallPopulation,totalSubsysPool,numOfSubsys_i_InOverallPopulation,ROCtotalSubsysPool) );
    end
    allPvals(:,j) = ROCpvals;
end

fid = fopen([dir 'enrichment_allPvals.tsv'],'w');
fprintf(fid,['KEGG_Map_Name' '\t' 'TP\tFP\tTA\tFA\tTTO\tFTO' '\n']);
for r = 1:length(usubsys)
    pvals2str = cell(1,length(totalROCScores));
    for c = 1:length(totalROCScores)
       pvals2str{c} = num2str(allPvals(r,c)); 
    end
    fprintf(fid,[usubsys{r} '\t' strjoin(pvals2str,'\t') '\n']);
end
fclose(fid);

%------------------------------------------------%
%                                                %
%------------------------------------------------%
end

